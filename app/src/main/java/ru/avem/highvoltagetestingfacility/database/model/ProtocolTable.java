package ru.avem.highvoltagetestingfacility.database.model;

public class ProtocolTable {
    static final String TABLE_NAME = "protocols";

    public static class COLUMN {
        static final String ID = "_id";
        static final String SERIAL_NUMBER = "serialNumber";
        static final String OBJECT_NAME = "objectName";
        static final String SPECIFIED_U = "specifiedU";
        static final String OBJECT_U_1 = "objectU1";
        static final String OBJECT_U_2 = "objectU2";
        static final String OBJECT_U_3 = "objectU3";
        static final String OBJECT_U_4 = "objectU4";
        static final String SPECIFIED_I = "specifiedI";
        static final String OBJECT_I_1 = "objectI1";
        static final String OBJECT_I_2 = "objectI2";
        static final String OBJECT_I_3 = "objectI3";
        static final String OBJECT_I_4 = "objectI4";
        static final String SPECIFIED_T = "specifiedT";
        static final String OBJECT_RESULT_1 = "objectResult1";
        static final String OBJECT_RESULT_2 = "objectResult2";
        static final String OBJECT_RESULT_3 = "objectResult3";
        static final String OBJECT_RESULT_4 = "objectResult4";
        static final String POSITION1 = "position1";
        static final String POSITION1_NUMBER = "position1Number";
        static final String POSITION1_FULL_NAME = "position1FullName";
        static final String POSITION2 = "position2";
        static final String POSITION2_NUMBER = "position2Number";
        static final String POSITION2_FULL_NAME = "position2FullName";
        static final String DATE = "date";
    }

    static final String CREATE_SCRIPT =
            String.format("CREATE TABLE %s ("
                            + "%s INTEGER PRIMARY KEY AUTOINCREMENT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s INTEGER,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s INTEGER,"
                            + "%s INTEGER,"
                            + "%s INTEGER,"
                            + "%s INTEGER,"
                            + "%s INTEGER,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s INTEGER" + ");",
                    TABLE_NAME,
                    COLUMN.ID,
                    COLUMN.SERIAL_NUMBER,
                    COLUMN.OBJECT_NAME,
                    COLUMN.SPECIFIED_U,
                    COLUMN.OBJECT_U_1,
                    COLUMN.OBJECT_U_2,
                    COLUMN.OBJECT_U_3,
                    COLUMN.OBJECT_U_4,
                    COLUMN.SPECIFIED_I,
                    COLUMN.OBJECT_I_1,
                    COLUMN.OBJECT_I_2,
                    COLUMN.OBJECT_I_3,
                    COLUMN.OBJECT_I_4,
                    COLUMN.SPECIFIED_T,
                    COLUMN.OBJECT_RESULT_1,
                    COLUMN.OBJECT_RESULT_2,
                    COLUMN.OBJECT_RESULT_3,
                    COLUMN.OBJECT_RESULT_4,
                    COLUMN.POSITION1,
                    COLUMN.POSITION1_NUMBER,
                    COLUMN.POSITION1_FULL_NAME,
                    COLUMN.POSITION2,
                    COLUMN.POSITION2_NUMBER,
                    COLUMN.POSITION2_FULL_NAME,
                    COLUMN.DATE);
}