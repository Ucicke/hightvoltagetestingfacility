package ru.avem.highvoltagetestingfacility.database.model;

import java.text.SimpleDateFormat;

public class Protocol {
    private long mId;
    private String mSerialNumber;
    private String mObjectName;
    private int mSpecifiedU;
    private float mObjectU1;
    private float mObjectU2;
    private float mObjectU3;
    private float mObjectU4;
    private float mSpecifiedI;
    private float mObjectI1;
    private float mObjectI2;
    private float mObjectI3;
    private float mObjectI4;
    private int mSpecifiedT;
    private int mObjectResult1;
    private int mObjectResult2;
    private int mObjectResult3;
    private int mObjectResult4;
    private String mPosition1;
    private String mPosition1Number;
    private String mPosition1FullName;
    private String mPosition2;
    private String mPosition2Number;
    private String mPosition2FullName;
    private long mDate;

    public Protocol(String serialNumber) {
        this(0, serialNumber, "",
                -1,
                -1f, -1f, -1f, -1f,
                -1f,
                -1f, -1f, -1f, -1f,
                -1,
                -1, -1, -1, -1,
                null, null, null,
                null, null, null,
                System.currentTimeMillis());
    }

    public Protocol(long id, String serialNumber, String objectName,
                    int specifiedU,
                    float objectU1, float objectU2, float objectU3, float objectU4,
                    float specifiedI,
                    float objectI1, float objectI2, float objectI3, float objectI4,
                    int specifiedT,
                    int objectResult1, int objectResult2, int objectResult3, int objectResult4,
                    String position1, String position1Number, String position1FullName,
                    String position2, String position2Number, String position2FullName,
                    long date) {
        mId = id;
        mSerialNumber = serialNumber;
        mObjectName = objectName;
        mSpecifiedU = specifiedU;
        mObjectU1 = objectU1;
        mObjectU2 = objectU2;
        mObjectU3 = objectU3;
        mObjectU4 = objectU4;
        mSpecifiedI = specifiedI;
        mObjectI1 = objectI1;
        mObjectI2 = objectI2;
        mObjectI3 = objectI3;
        mObjectI4 = objectI4;
        mSpecifiedT = specifiedT;
        mObjectResult1 = objectResult1;
        mObjectResult2 = objectResult2;
        mObjectResult3 = objectResult3;
        mObjectResult4 = objectResult4;
        mPosition1 = position1;
        mPosition1Number = position1Number;
        mPosition1FullName = position1FullName;
        mPosition2 = position2;
        mPosition2Number = position2Number;
        mPosition2FullName = position2FullName;
        mDate = date;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getSerialNumber() {
        return mSerialNumber;
    }

    public String getObjectName() {
        return mObjectName;
    }

    public void setObjectName(String objectName) {
        mObjectName = objectName;
    }

    public int getSpecifiedU() {
        return mSpecifiedU;
    }

    public void setSpecifiedU(int specifiedU) {
        mSpecifiedU = specifiedU;
    }

    public float getObjectU1() {
        return mObjectU1;
    }

    public void setObjectU1(float objectU1) {
        mObjectU1 = objectU1;
    }

    public float getObjectU2() {
        return mObjectU2;
    }

    public void setObjectU2(float objectU2) {
        mObjectU2 = objectU2;
    }

    public float getObjectU3() {
        return mObjectU3;
    }

    public void setObjectU3(float objectU3) {
        mObjectU3 = objectU3;
    }

    public float getObjectU4() {
        return mObjectU4;
    }

    public void setObjectU4(float objectU4) {
        mObjectU4 = objectU4;
    }

    public float getSpecifiedI() {
        return mSpecifiedI;
    }

    public void setSpecifiedI(float specifiedI) {
        mSpecifiedI = specifiedI;
    }

    public float getObjectI1() {
        return mObjectI1;
    }

    public void setObjectI1(float objectI1) {
        mObjectI1 = objectI1;
    }

    public float getObjectI2() {
        return mObjectI2;
    }

    public void setObjectI2(float objectI2) {
        mObjectI2 = objectI2;
    }

    public float getObjectI3() {
        return mObjectI3;
    }

    public void setObjectI3(float objectI3) {
        mObjectI3 = objectI3;
    }

    public float getObjectI4() {
        return mObjectI4;
    }

    public void setObjectI4(float objectI4) {
        mObjectI4 = objectI4;
    }

    public int getSpecifiedT() {
        return mSpecifiedT;
    }

    public void setSpecifiedT(int specifiedT) {
        mSpecifiedT = specifiedT;
    }

    public int getObjectResult1() {
        return mObjectResult1;
    }

    public void setObjectResult1(int objectResult1) {
        mObjectResult1 = objectResult1;
    }

    public int getObjectResult2() {
        return mObjectResult2;
    }

    public void setObjectResult2(int objectResult2) {
        mObjectResult2 = objectResult2;
    }

    public int getObjectResult3() {
        return mObjectResult3;
    }

    public void setObjectResult3(int objectResult3) {
        mObjectResult3 = objectResult3;
    }

    public int getObjectResult4() {
        return mObjectResult4;
    }

    public void setObjectResult4(int objectResult4) {
        mObjectResult4 = objectResult4;
    }

    public String getPosition1() {
        return mPosition1;
    }

    public void setPosition1(String position1) {
        mPosition1 = position1;
    }

    public String getPosition1Number() {
        return mPosition1Number;
    }

    public void setPosition1Number(String position1Number) {
        mPosition1Number = position1Number;
    }

    public String getPosition1FullName() {
        return mPosition1FullName;
    }

    public void setPosition1FullName(String position1FullName) {
        mPosition1FullName = position1FullName;
    }

    public String getPosition2() {
        return mPosition2;
    }

    public void setPosition2(String position2) {
        mPosition2 = position2;
    }

    public String getPosition2Number() {
        return mPosition2Number;
    }

    public void setPosition2Number(String position2Number) {
        mPosition2Number = position2Number;
    }

    public String getPosition2FullName() {
        return mPosition2FullName;
    }

    public void setPosition2FullName(String position2FullName) {
        mPosition2FullName = position2FullName;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("Время проведения испытания: HH:mm:ss");
        return String.format("%s. № %s (%s) %s", mId, mSerialNumber, mObjectName, sdf.format(mDate));
    }
}
