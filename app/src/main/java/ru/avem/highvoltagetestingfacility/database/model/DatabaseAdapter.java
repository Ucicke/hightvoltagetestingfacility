package ru.avem.highvoltagetestingfacility.database.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAdapter {
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDatabase;

    public DatabaseAdapter(Context context) {
        mDbHelper = new DatabaseHelper(context);
    }

    public DatabaseAdapter open() {
        mDatabase = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public List<Protocol> getProtocols() {
        ArrayList<Protocol> protocols = new ArrayList<>();
        Cursor cursor = getAllProtocols();
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.ID));
                String serialNumber = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SERIAL_NUMBER));
                String objectName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_NAME));
                int specifiedU = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U));
                float objectU1 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_1));
                float objectU2 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_2));
                float objectU3 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_3));
                float objectU4 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_4));
                float specifiedI = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I));
                float objectI1 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_1));
                float objectI2 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_2));
                float objectI3 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_3));
                float objectI4 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_4));
                int specifiedT = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_T));
                int objectResult1 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_1));
                int objectResult2 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_2));
                int objectResult3 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_3));
                int objectResult4 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_4));
                String position1 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1));
                String position1Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_NUMBER));
                String position1FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_FULL_NAME));
                String position2 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2));
                String position2Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_NUMBER));
                String position2FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_FULL_NAME));
                long date = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.DATE));
                protocols.add(new Protocol(id, serialNumber, objectName,
                        specifiedU,
                        objectU1, objectU2, objectU3, objectU4,
                        specifiedI,
                        objectI1, objectI2, objectI3, objectI4,
                        specifiedT,
                        objectResult1, objectResult2, objectResult3, objectResult4,
                        position1, position1Number, position1FullName,
                        position2, position2Number, position2FullName,
                        date));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return protocols;
    }

    private Cursor getAllProtocols() {
        String[] columns = new String[]{
                ProtocolTable.COLUMN.ID,
                ProtocolTable.COLUMN.SERIAL_NUMBER,
                ProtocolTable.COLUMN.OBJECT_NAME,
                ProtocolTable.COLUMN.SPECIFIED_U,
                ProtocolTable.COLUMN.OBJECT_U_1,
                ProtocolTable.COLUMN.OBJECT_U_2,
                ProtocolTable.COLUMN.OBJECT_U_3,
                ProtocolTable.COLUMN.OBJECT_U_4,
                ProtocolTable.COLUMN.SPECIFIED_I,
                ProtocolTable.COLUMN.OBJECT_I_1,
                ProtocolTable.COLUMN.OBJECT_I_2,
                ProtocolTable.COLUMN.OBJECT_I_3,
                ProtocolTable.COLUMN.OBJECT_I_4,
                ProtocolTable.COLUMN.SPECIFIED_T,
                ProtocolTable.COLUMN.OBJECT_RESULT_1,
                ProtocolTable.COLUMN.OBJECT_RESULT_2,
                ProtocolTable.COLUMN.OBJECT_RESULT_3,
                ProtocolTable.COLUMN.OBJECT_RESULT_4,
                ProtocolTable.COLUMN.POSITION1,
                ProtocolTable.COLUMN.POSITION1_NUMBER,
                ProtocolTable.COLUMN.POSITION1_FULL_NAME,
                ProtocolTable.COLUMN.POSITION2,
                ProtocolTable.COLUMN.POSITION2_NUMBER,
                ProtocolTable.COLUMN.POSITION2_FULL_NAME,
                ProtocolTable.COLUMN.DATE};
        return mDatabase.query(ProtocolTable.TABLE_NAME, columns, null, null, null, null, null);
    }

    public List<Protocol> getProtocolsByDate(long startDate, long endDate) {
        ArrayList<Protocol> protocols = new ArrayList<>();
        Cursor cursor = getAllProtocolsByDate(startDate, endDate);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.ID));
                String serialNumber = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SERIAL_NUMBER));
                String objectName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_NAME));
                int specifiedU = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U));
                float objectU1 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_1));
                float objectU2 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_2));
                float objectU3 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_3));
                float objectU4 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_4));
                float specifiedI = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I));
                float objectI1 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_1));
                float objectI2 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_2));
                float objectI3 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_3));
                float objectI4 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_4));
                int specifiedT = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_T));
                int objectResult1 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_1));
                int objectResult2 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_2));
                int objectResult3 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_3));
                int objectResult4 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_4));
                String position1 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1));
                String position1Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_NUMBER));
                String position1FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_FULL_NAME));
                String position2 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2));
                String position2Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_NUMBER));
                String position2FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_FULL_NAME));
                long date = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.DATE));
                protocols.add(new Protocol(id, serialNumber, objectName,
                        specifiedU,
                        objectU1, objectU2, objectU3, objectU4,
                        specifiedI,
                        objectI1, objectI2, objectI3, objectI4,
                        specifiedT,
                        objectResult1, objectResult2, objectResult3, objectResult4,
                        position1, position1Number, position1FullName,
                        position2, position2Number, position2FullName,
                        date));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return protocols;
    }

    private Cursor getAllProtocolsByDate(long startDate, long endDate) {
        String[] columns = new String[]{
                ProtocolTable.COLUMN.ID,
                ProtocolTable.COLUMN.SERIAL_NUMBER,
                ProtocolTable.COLUMN.OBJECT_NAME,
                ProtocolTable.COLUMN.SPECIFIED_U,
                ProtocolTable.COLUMN.OBJECT_U_1,
                ProtocolTable.COLUMN.OBJECT_U_2,
                ProtocolTable.COLUMN.OBJECT_U_3,
                ProtocolTable.COLUMN.OBJECT_U_4,
                ProtocolTable.COLUMN.SPECIFIED_I,
                ProtocolTable.COLUMN.OBJECT_I_1,
                ProtocolTable.COLUMN.OBJECT_I_2,
                ProtocolTable.COLUMN.OBJECT_I_3,
                ProtocolTable.COLUMN.OBJECT_I_4,
                ProtocolTable.COLUMN.SPECIFIED_T,
                ProtocolTable.COLUMN.OBJECT_RESULT_1,
                ProtocolTable.COLUMN.OBJECT_RESULT_2,
                ProtocolTable.COLUMN.OBJECT_RESULT_3,
                ProtocolTable.COLUMN.OBJECT_RESULT_4,
                ProtocolTable.COLUMN.POSITION1,
                ProtocolTable.COLUMN.POSITION1_NUMBER,
                ProtocolTable.COLUMN.POSITION1_FULL_NAME,
                ProtocolTable.COLUMN.POSITION2,
                ProtocolTable.COLUMN.POSITION2_NUMBER,
                ProtocolTable.COLUMN.POSITION2_FULL_NAME,
                ProtocolTable.COLUMN.DATE};
        String selection = "date > ? AND date < ?";
        String[] selectionArgs = new String[] { startDate + "", endDate + "" };
        return mDatabase.query(ProtocolTable.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
    }

    public Protocol getProtocol(long id) {
        Protocol protocol = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?", ProtocolTable.TABLE_NAME, ProtocolTable.COLUMN.ID);
        Cursor cursor = mDatabase.rawQuery(query, new String[]{String.valueOf(id)});
        if (cursor.moveToFirst()) {
            String serialNumber = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SERIAL_NUMBER));
            String objectName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_NAME));
            int specifiedU = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U));
            float objectU1 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_1));
            float objectU2 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_2));
            float objectU3 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_3));
            float objectU4 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_4));
            float specifiedI = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I));
            float objectI1 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_1));
            float objectI2 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_2));
            float objectI3 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_3));
            float objectI4 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_4));
            int specifiedT = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_T));
            int objectResult1 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_1));
            int objectResult2 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_2));
            int objectResult3 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_3));
            int objectResult4 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_4));
            String position1 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1));
            String position1Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_NUMBER));
            String position1FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_FULL_NAME));
            String position2 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2));
            String position2Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_NUMBER));
            String position2FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_FULL_NAME));
            long date = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.DATE));
            protocol = (new Protocol(id, serialNumber, objectName,
                    specifiedU,
                    objectU1, objectU2, objectU3, objectU4,
                    specifiedI,
                    objectI1, objectI2, objectI3, objectI4,
                    specifiedT,
                    objectResult1, objectResult2, objectResult3, objectResult4,
                    position1, position1Number, position1FullName,
                    position2, position2Number, position2FullName,
                    date));
        }
        cursor.close();
        return protocol;
    }

    public Protocol getProtocol(String protocolName) {
        Protocol protocol = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?", ProtocolTable.TABLE_NAME, ProtocolTable.COLUMN.SERIAL_NUMBER);
        Cursor cursor = mDatabase.rawQuery(query, new String[]{String.valueOf(protocolName)});
        if (cursor.moveToFirst()) {
            long id = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.ID));
            String serialNumber = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SERIAL_NUMBER));
            String objectName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_NAME));
            int specifiedU = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U));
            float objectU1 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_1));
            float objectU2 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_2));
            float objectU3 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_3));
            float objectU4 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_4));
            float specifiedI = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I));
            float objectI1 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_1));
            float objectI2 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_2));
            float objectI3 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_3));
            float objectI4 = cursor.getFloat(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_4));
            int specifiedT = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_T));
            int objectResult1 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_1));
            int objectResult2 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_2));
            int objectResult3 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_3));
            int objectResult4 = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_RESULT_4));
            String position1 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1));
            String position1Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_NUMBER));
            String position1FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_FULL_NAME));
            String position2 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2));
            String position2Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_NUMBER));
            String position2FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_FULL_NAME));
            long date = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.DATE));
            protocol = (new Protocol(id, serialNumber, objectName,
                    specifiedU,
                    objectU1, objectU2, objectU3, objectU4,
                    specifiedI,
                    objectI1, objectI2, objectI3, objectI4,
                    specifiedT,
                    objectResult1, objectResult2, objectResult3, objectResult4,
                    position1, position1Number, position1FullName,
                    position2, position2Number, position2FullName,
                    date));
        }
        cursor.close();
        return protocol;
    }

    public long insertProtocol(Protocol protocol) {
        ContentValues cv = new ContentValues();
        cv.put(ProtocolTable.COLUMN.SERIAL_NUMBER, protocol.getSerialNumber());
        cv.put(ProtocolTable.COLUMN.OBJECT_NAME, protocol.getObjectName());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U, protocol.getSpecifiedU());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_1, protocol.getObjectU1());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_2, protocol.getObjectU2());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_3, protocol.getObjectU3());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_4, protocol.getObjectU4());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_I, protocol.getSpecifiedI());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_1, protocol.getObjectI1());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_2, protocol.getObjectI2());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_3, protocol.getObjectI3());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_4, protocol.getObjectI4());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_T, protocol.getSpecifiedT());
        cv.put(ProtocolTable.COLUMN.OBJECT_RESULT_1, protocol.getObjectResult1());
        cv.put(ProtocolTable.COLUMN.OBJECT_RESULT_2, protocol.getObjectResult2());
        cv.put(ProtocolTable.COLUMN.OBJECT_RESULT_3, protocol.getObjectResult3());
        cv.put(ProtocolTable.COLUMN.OBJECT_RESULT_4, protocol.getObjectResult4());
        cv.put(ProtocolTable.COLUMN.POSITION1, protocol.getPosition1());
        cv.put(ProtocolTable.COLUMN.POSITION1_NUMBER, protocol.getPosition1Number());
        cv.put(ProtocolTable.COLUMN.POSITION1_FULL_NAME, protocol.getPosition1FullName());
        cv.put(ProtocolTable.COLUMN.POSITION2, protocol.getPosition2());
        cv.put(ProtocolTable.COLUMN.POSITION2_NUMBER, protocol.getPosition2Number());
        cv.put(ProtocolTable.COLUMN.POSITION2_FULL_NAME, protocol.getPosition2FullName());
        cv.put(ProtocolTable.COLUMN.DATE, protocol.getDate());
        return mDatabase.insert(ProtocolTable.TABLE_NAME, null, cv);
    }

    public long deleteProtocol(long protocolId) {
        String whereClause = "_id = ?";
        String[] whereArgs = new String[]{String.valueOf(protocolId)};
        return mDatabase.delete(ProtocolTable.TABLE_NAME, whereClause, whereArgs);
    }

    public long updateProtocol(Protocol protocol) {
        String whereClause = ProtocolTable.COLUMN.ID + "=" + String.valueOf(protocol.getId());
        ContentValues cv = new ContentValues();
        cv.put(ProtocolTable.COLUMN.SERIAL_NUMBER, protocol.getSerialNumber());
        cv.put(ProtocolTable.COLUMN.OBJECT_NAME, protocol.getObjectName());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U, protocol.getSpecifiedU());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_1, protocol.getObjectU1());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_2, protocol.getObjectU2());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_3, protocol.getObjectU3());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_4, protocol.getObjectU4());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_I, protocol.getSpecifiedI());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_1, protocol.getObjectI1());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_2, protocol.getObjectI2());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_3, protocol.getObjectI3());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_4, protocol.getObjectI4());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_T, protocol.getSpecifiedT());
        cv.put(ProtocolTable.COLUMN.OBJECT_RESULT_1, protocol.getObjectResult1());
        cv.put(ProtocolTable.COLUMN.OBJECT_RESULT_2, protocol.getObjectResult2());
        cv.put(ProtocolTable.COLUMN.OBJECT_RESULT_3, protocol.getObjectResult3());
        cv.put(ProtocolTable.COLUMN.OBJECT_RESULT_4, protocol.getObjectResult4());
        cv.put(ProtocolTable.COLUMN.POSITION1, protocol.getPosition1());
        cv.put(ProtocolTable.COLUMN.POSITION1_NUMBER, protocol.getPosition1Number());
        cv.put(ProtocolTable.COLUMN.POSITION1_FULL_NAME, protocol.getPosition1FullName());
        cv.put(ProtocolTable.COLUMN.POSITION2, protocol.getPosition2());
        cv.put(ProtocolTable.COLUMN.POSITION2_NUMBER, protocol.getPosition2Number());
        cv.put(ProtocolTable.COLUMN.POSITION2_FULL_NAME, protocol.getPosition2FullName());
        cv.put(ProtocolTable.COLUMN.DATE, protocol.getDate());
        return mDatabase.update(ProtocolTable.TABLE_NAME, cv, whereClause, null);
    }
}