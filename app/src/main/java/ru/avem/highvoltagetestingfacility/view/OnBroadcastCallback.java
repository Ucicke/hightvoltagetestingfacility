package ru.avem.highvoltagetestingfacility.view;

import android.content.BroadcastReceiver;

public interface OnBroadcastCallback {
    void onBroadcastUsbReceiver(BroadcastReceiver broadcastReceiver);
}
