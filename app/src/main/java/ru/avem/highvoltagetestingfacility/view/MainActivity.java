package ru.avem.highvoltagetestingfacility.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import ru.avem.highvoltagetestingfacility.R;
import ru.avem.highvoltagetestingfacility.database.model.Protocol;
import ru.avem.highvoltagetestingfacility.logging.Logging;
import ru.avem.highvoltagetestingfacility.model.MainModel;
import ru.avem.highvoltagetestingfacility.presenter.ControlPanelPresenter;
import ru.avem.highvoltagetestingfacility.presenter.ControlPanelView;
import ru.avem.highvoltagetestingfacility.presenter.MainPresenter;
import ru.avem.highvoltagetestingfacility.presenter.MainPresenterView;
import ru.avem.highvoltagetestingfacility.utils.Logger;

import static android.text.TextUtils.isEmpty;
import static ru.avem.highvoltagetestingfacility.model.MainModel.REFRESH_LOG;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.BREAKDOWN_RESULT;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.CONC_UP_RESULT;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_1;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_2;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_3;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_4;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_5;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_6;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_7;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_8;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_9;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_I_1;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_I_2;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_I_3;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_I_4;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_RESULT_1;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_RESULT_2;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_RESULT_3;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_RESULT_4;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_T_1;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_T_2;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_T_3;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_T_4;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_U_1;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_U_2;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_U_3;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OBJECT_U_4;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.OK_RESULT;
import static ru.avem.highvoltagetestingfacility.utils.Visibility.addTabToTabHost;
import static ru.avem.highvoltagetestingfacility.utils.Visibility.disableView;
import static ru.avem.highvoltagetestingfacility.utils.Visibility.enableView;
import static ru.avem.highvoltagetestingfacility.utils.Visibility.setViewAndChildrenEnabled;
import static ru.avem.highvoltagetestingfacility.utils.Visibility.setViewAndChildrenVisibility;

public class MainActivity extends AppCompatActivity implements MainPresenterView, ControlPanelView, Observer {
    //region Константы вкладок
    private static final String OBJECT_TAB_TAG = "Object";
    private static final int OBJECT_VIEW_ID = R.id.tab_object;
    private static final String OBJECT_TAB_LABEL = "Объект";
    private static final int OBJECT_TAB_INDEX = 0;

    private static final String EXPERIMENTS_TAB_TAG = "Experiments";
    private static final int EXPERIMENTS_VIEW_ID = R.id.tab_experiments;
    private static final String EXPERIMENTS_TAB_LABEL = "Испытания";
    private static final int EXPERIMENTS_TAB_INDEX = 1;

    private static final String RESULTS_TAB_TAG = "Results";
    private static final int RESULTS_VIEW_ID = R.id.tab_results;
    private static final String RESULTS_TAB_LABEL = "Результаты";
    private static final int RESULT_TAB_INDEX = 2;

    private static final String PROTOCOL_TAB_TAG = "Protocol";
    private static final int PROTOCOL_VIEW_ID = R.id.tab_protocol;
    private static final String PROTOCOL_TAB_LABEL = "Протокол";
    private static final int PROTOCOL_TAB_INDEX = 3;
    //endregion

    //region Константные поля
    private final MainModel mMainModel = new MainModel(this);
    private MainPresenter mMainPresenter;
    private final ControlPanelPresenter mControlPanelPresenter = new ControlPanelPresenter(this);
    private final Handler mHandler = new Handler();
    private final SimpleDateFormat mFormat = new SimpleDateFormat("dd-MM-yyyy");
    private OnBroadcastCallback mOnBroadcastCallback = new OnBroadcastCallback() {
        @Override
        public void onBroadcastUsbReceiver(BroadcastReceiver broadcastReceiver) {
            mBroadcastReceiver = broadcastReceiver;
        }
    };
    private BroadcastReceiver mBroadcastReceiver;
    //endregion

    //region Виджеты

    //region Заголовочные
    @BindView(R.id.exit)
    TextView mExit;
    @BindView(R.id.main_title)
    TextView mMainTitle;
    @BindView(R.id.object_title)
    TextView mObjectTitle;
    @BindView(R.id.serial_number_title)
    TextView mSerialNumberTitle;
    //endregion

    //region Управление вкладками
    @BindView(R.id.tab_host)
    TabHost mTabHost;
    @BindView(android.R.id.tabs)
    TabWidget mTabs;
    //endregion

    //region Вкладка 1 Выбор объекта
    @BindView(R.id.serial_number)
    EditText mSerialNumber;
    @BindView(R.id.serial_number_enter)
    ToggleButton mSerialNumberEnter;
    @BindView(R.id.objects_selector_title)
    TextView mObjectsSelectorTitle;
    @BindView(R.id.objects_selector)
    Spinner mObjectsSelector;
    @BindView(R.id.specified_params_title)
    TextView mSpecifiedParamsTitle;
    @BindView(R.id.specified_params)
    TableLayout mSpecifiedParams;
    @BindView(R.id.specified_u)
    EditText mSpecifiedU;
    @BindView(R.id.specified_i)
    EditText mSpecifiedI;
    @BindView(R.id.specified_t)
    EditText mSpecifiedT;
    @BindView(R.id.electrode_title)
    TextView mElectrodeTitle;
    @BindView(R.id.electrodes)
    LinearLayout mElectrodes;
    @BindView(R.id.electrode1)
    CheckBox mElectrode1;
    @BindView(R.id.electrode2)
    CheckBox mElectrode2;
    @BindView(R.id.electrode3)
    CheckBox mElectrode3;
    @BindView(R.id.electrode4)
    CheckBox mElectrode4;


    @BindView(R.id.object_enter)
    Button mObjectEnter;
    @BindView(R.id.object_cancel)
    Button mObjectCancel;
    @BindView(R.id.object_next)
    Button mObjectNext;
    //endregion

    //region Вкладка 2 Испытания
    @BindView(R.id.tab_experiments)
    LinearLayout mTabExperiments;
    //endregion

    //region Вкладка 3 Результаты
    @BindView(R.id.table_experiment1)
    TableLayout mTableExperiment1;
    @BindView(R.id.table_experiment2)
    TableLayout mTableExperiment2;
    @BindView(R.id.table_experiment3)
    TableLayout mTableExperiment3;
    @BindView(R.id.table_experiment4)
    TableLayout mTableExperiment4;

    @BindView(R.id.parameter_1_1)
    TextView mParameter1_1;
    @BindView(R.id.parameter_1_2)
    TextView mParameter1_2;
    @BindView(R.id.parameter_1_3)
    TextView mParameter1_3;
    @BindView(R.id.parameter_1_4)
    TextView mParameter1_4;
    @BindView(R.id.parameter_1_5)
    TextView mParameter1_5;
    @BindView(R.id.parameter_1_6)
    TextView mParameter1_6;
    @BindView(R.id.parameter_2_1)
    TextView mParameter2_1;
    @BindView(R.id.parameter_2_2)
    TextView mParameter2_2;
    @BindView(R.id.parameter_2_3)
    TextView mParameter2_3;
    @BindView(R.id.parameter_2_4)
    TextView mParameter2_4;
    @BindView(R.id.parameter_2_5)
    TextView mParameter2_5;
    @BindView(R.id.parameter_2_6)
    TextView mParameter2_6;
    @BindView(R.id.parameter_3_1)
    TextView mParameter3_1;
    @BindView(R.id.parameter_3_2)
    TextView mParameter3_2;
    @BindView(R.id.parameter_3_3)
    TextView mParameter3_3;
    @BindView(R.id.parameter_3_4)
    TextView mParameter3_4;
    @BindView(R.id.parameter_3_5)
    TextView mParameter3_5;
    @BindView(R.id.parameter_3_6)
    TextView mParameter3_6;
    @BindView(R.id.parameter_4_1)
    TextView mParameter4_1;
    @BindView(R.id.parameter_4_2)
    TextView mParameter4_2;
    @BindView(R.id.parameter_4_3)
    TextView mParameter4_3;
    @BindView(R.id.parameter_4_4)
    TextView mParameter4_4;
    @BindView(R.id.parameter_4_5)
    TextView mParameter4_5;
    @BindView(R.id.parameter_4_6)
    TextView mParameter4_6;

    @BindView(R.id.fix_result)
    TextView mFixResult;

    @BindView(R.id.connection_warning)
    LinearLayout mConnectionWarning;
    //endregion

    //region Вкладка 4 Протокол
    @BindView(R.id.review_layout)
    GridLayout mReviewLayout;

    @BindView(R.id.select_date)
    Button mSelectDate;
    @BindView(R.id.protocols)
    Spinner mProtocols;

    @BindView(R.id.save_layout)
    GridLayout mSaveLayout;


    @BindView(R.id.position1)
    Spinner mPosition1;
    @BindView(R.id.position1_number)
    EditText mPosition1Number;
    @BindView(R.id.position1_full_name)
    EditText mPosition1FullName;
    @BindView(R.id.position2)
    Spinner mPosition2;
    @BindView(R.id.position2_number)
    EditText mPosition2Number;
    @BindView(R.id.position2_full_name)
    EditText mPosition2FullName;
    //endregion

    //region Панель управления
    @BindView(R.id.option_selection_layout)
    LinearLayout mOptionSelectionLayout;
    @BindView(R.id.switcher)
    ToggleButton mSwitcher;
    @BindView(R.id.devices_state_panel)
    LinearLayout mDevicesStatePanel;
    @BindView(R.id.log)
    TextView mLog;
    @BindView(R.id.log_state_panel)
    LinearLayout mLogStatePanel;

    //region Состояние приборов
    @BindView(R.id.sheem)
    LinearLayout mSheem;
    @BindView(R.id.sheem_responding)
    TextView mSheemResponding;
    @BindView(R.id.sheem_reconnect)
    Button mSheemReconnect;

    @BindView(R.id.voltmeter)
    LinearLayout mVoltmeter;
    @BindView(R.id.voltmeter_responding)
    TextView mVoltmeterResponding;
    @BindView(R.id.voltmeter_reconnect)
    Button mVoltmeterReconnect;

    @BindView(R.id.ammeter)
    LinearLayout mAmmeter;
    @BindView(R.id.ammeter_responding)
    TextView mAmmeterResponding;
    @BindView(R.id.ammeter_reconnect)
    Button mAmmeterReconnect;

    @BindView(R.id.pr)
    LinearLayout mPr;
    @BindView(R.id.pr_responding)
    TextView mPRResponding;
    @BindView(R.id.pr_reconnect)
    Button mPRReconnect;

    @BindView(R.id.pm130)
    LinearLayout mPM130;
    @BindView(R.id.pm130_responding)
    TextView mPM130Responding;
    @BindView(R.id.pm130_reconnect)
    TextView mPM130Reconnect;

    @BindView(R.id.ohmmeter)
    LinearLayout mOhmmeter;
    @BindView(R.id.ohmmeter_responding)
    TextView mOhmmeterResponding;
    @BindView(R.id.ohmmeter_reconnect)
    TextView mOhmmeterReconnect;
    //endregion
    //endregion

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");
        ButterKnife.bind(this);

        mMainPresenter = new MainPresenter(this, mMainModel, mOnBroadcastCallback, this);
        mMainPresenter.activityReady();
    }

    private long getStartDate(String s) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(mFormat.parse(s));
        } catch (ParseException ignored) {
        }
        return cal.getTimeInMillis();
    }

    private long getEndDate(String s) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(mFormat.parse(s));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        } catch (ParseException ignored) {
        }
        return cal.getTimeInMillis();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void initializeViews() {
        initializeTabHost();
        initializeObjectSelector();
        hideDevicesStates();
        initializeSpecifiedParamsTables();
        initializeDateButton();
        mPosition2.setSelection(1);
    }

    private void initializeTabHost() {
        mTabHost.setup();
        addTabToTabHost(mTabHost, OBJECT_TAB_TAG, OBJECT_VIEW_ID, OBJECT_TAB_LABEL);
        addTabToTabHost(mTabHost, EXPERIMENTS_TAB_TAG, EXPERIMENTS_VIEW_ID, EXPERIMENTS_TAB_LABEL);
        addTabToTabHost(mTabHost, RESULTS_TAB_TAG, RESULTS_VIEW_ID, RESULTS_TAB_LABEL);
        addTabToTabHost(mTabHost, PROTOCOL_TAB_TAG, PROTOCOL_VIEW_ID, PROTOCOL_TAB_LABEL);
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tag) {
                switch (tag) {
                    case RESULTS_TAB_TAG:
                        mMainPresenter.resultsTabSelected();
                        break;
                    case PROTOCOL_TAB_TAG:
                        mMainPresenter.protocolTabSelected(MainActivity.this, mProtocols, getStartDate(mSelectDate.getText().toString()), getEndDate(mSelectDate.getText().toString()));
                        break;
                }
            }
        });
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            TextView tabTextView = mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tabTextView.setTextColor(Color.parseColor("#000000"));
        }
    }

    @Override
    public void showConnectionWarning() {
        mConnectionWarning.setVisibility(View.GONE);
    }

    @Override
    public void showSaveLayout() {
        mReviewLayout.setVisibility(View.GONE);
        mSaveLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showReviewLayout() {
        mSaveLayout.setVisibility(View.GONE);
        mReviewLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void initializeObjectSelector() {
        ArrayAdapter<CharSequence> arrayAdapterFromResource = ArrayAdapter.createFromResource(
                this, R.array.experiments, R.layout.spinner_layout);
        mObjectsSelector.setAdapter(arrayAdapterFromResource);
        hideObjects();
    }

    private void hideDevicesStates() {
        mSheem.setVisibility(View.GONE);
        mVoltmeter.setVisibility(View.GONE);
        mAmmeter.setVisibility(View.GONE);
        mPr.setVisibility(View.GONE);
        mPM130.setVisibility(View.GONE);
        mOhmmeter.setVisibility(View.GONE);
    }

    @OnItemSelected(R.id.protocols)
    public void onProtocolSelected(Spinner view) {
        mMainPresenter.setProtocolForInteraction((Protocol) view.getSelectedItem());
    }

    @Override
    public void finishApplication() {
        System.exit(0);
    }

    @Override
    public boolean isControlPanelDisplayed() {
        return !mSwitcher.isChecked();
    }

    @Override
    public void hideControlPanel() {
        mMainTitle.setText("Состояние объекта испытания");
        mOptionSelectionLayout.setVisibility(View.GONE);
        mDevicesStatePanel.setVisibility(View.GONE);
        mLogStatePanel.setVisibility(View.GONE);
        mTabHost.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayControlPanel() {
        mMainTitle.setText("Панель управления");
        mTabHost.setVisibility(View.GONE);
        mOptionSelectionLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayDevicesStatePanel() {
        mDevicesStatePanel.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayLogStatePanel() {
        mLogStatePanel.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
    }

    @OnCheckedChanged(R.id.serial_number_enter)
    public void onCheckedChanged(boolean state) {
        mMainPresenter.serialNumberEnterClicked(state);
    }

    @OnClick({R.id.exit,
            R.id.serial_number_enter,
            R.id.object_enter,
            R.id.object_cancel,
            R.id.object_next,
            R.id.fix_result,
            R.id.protection_status,
            R.id.subject,
            R.id.events,
            R.id.devices_state,
            R.id.data_transfer,
            R.id.switcher,
            R.id.save,
            R.id.preview,
            R.id.save_on_flash})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.exit:
                mMainPresenter.exitPressed();
//                mHandler.postDelayed(new Runnable() {//
//                    @Override
//                    public void run() {
//                        mMainModel.setObj1State(false);
//                    }
//                }, 1000);
//                mHandler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mMainModel.setObj2State(false);
//                    }
//                }, 2000);
//                mHandler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mMainModel.setObj3State(false);
//                    }
//                }, 3000);
//                mHandler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mMainModel.setObj4State(false);
//                    }
//                }, 4000);
//                mMainPresenter.showObjectParameters();
                break;
            case R.id.object_enter:
                mMainPresenter.objectEnter();
                break;
            case R.id.object_cancel:
                mMainPresenter.objectCancel();
                uncheckSerialNumberEnter();
                break;
            case R.id.object_next:
                mMainPresenter.objectNext();
                break;
            case R.id.fix_result:
                mMainPresenter.fixResult();
                break;
            case R.id.protection_status:
                mControlPanelPresenter.protectionStatusClicked();
                break;
            case R.id.subject:
                mControlPanelPresenter.subjectClicked();
                break;
            case R.id.events:
                mControlPanelPresenter.eventsClicked();
                break;
            case R.id.devices_state:
//                mControlPanelPresenter.devicesStateClicked();
                break;
            case R.id.data_transfer:
                mControlPanelPresenter.dataTransferClicked();
                break;
            case R.id.switcher:
                mControlPanelPresenter.switcherClicked();
                break;
            case R.id.save:
                if (fieldsAreFilled()) {
                    mMainPresenter.saveProtocolInDB(
                            (String) mPosition1.getSelectedItem(),
                            mPosition1Number.getText().toString(),
                            mPosition1FullName.getText().toString(),
                            (String) mPosition2.getSelectedItem(),
                            mPosition2Number.getText().toString(),
                            mPosition2FullName.getText().toString(),
                            System.currentTimeMillis());
                    mMainPresenter.setNeedToSave(false);
                    Toast.makeText(this, "Сохранено", Toast.LENGTH_LONG).show();
                    mMainPresenter.protocolTabSelected(MainActivity.this, mProtocols, getStartDate(mSelectDate.getText().toString()), getEndDate(mSelectDate.getText().toString()));
                    mMainPresenter.objectCancel();
                    uncheckSerialNumberEnter();
                } else {
                    Toast.makeText(this, "Заполните все поля", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.preview:
                Logging.preview(this, mMainPresenter.getProtocolForInteraction());
                break;
            case R.id.save_on_flash:
                Logging.saveFileOnFlashMassStorage(this, mMainPresenter.getProtocolForInteraction());
                break;
        }
    }

    private boolean fieldsAreFilled() {
        return !isEmpty(mPosition1Number.getText().toString()) &&
                !isEmpty(mPosition1FullName.getText().toString()) &&
                !isEmpty(mPosition2Number.getText().toString()) &&
                !isEmpty(mPosition2FullName.getText().toString());
    }

    @OnItemSelected(R.id.objects_selector)
    public void onObjectSelected(View view) {
        mMainPresenter.objectSelected(((TextView) view).getText().toString());
        Logger.withTag("DEBUG_TAG").log(((TextView) view).getText().toString());
    }

    @Override
    public String getSerialNumber() {
        return mSerialNumber.getText().toString();
    }

    @Override
    public void showObjects(String serialNumber) {
        mSerialNumberTitle.setText(serialNumber);
        disableView(mSerialNumber);
        mObjectsSelectorTitle.setVisibility(View.VISIBLE);
        mObjectsSelector.setVisibility(View.VISIBLE);
        mSpecifiedParamsTitle.setVisibility(View.VISIBLE);
        mSpecifiedParams.setVisibility(View.VISIBLE);
        mObjectEnter.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideObjects() {
        mSerialNumberTitle.setText("");
        enableView(mSerialNumber);
        mObjectsSelectorTitle.setVisibility(View.GONE);
        mObjectsSelector.setVisibility(View.GONE);
        mSpecifiedParamsTitle.setVisibility(View.GONE);
        mSpecifiedParams.setVisibility(View.GONE);
        mObjectEnter.setVisibility(View.GONE);
        mElectrodeTitle.setVisibility(View.GONE);
        mElectrodes.setVisibility(View.GONE);
    }

    @Override
    public void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void uncheckSerialNumberEnter() {
        mSerialNumberEnter.setChecked(false);
    }

    @Override
    public void setSpecifiedParam(String object, int specifiedU, float specifiedI, int specifiedT) {
        mElectrodeTitle.setVisibility(View.GONE);
        mElectrodes.setVisibility(View.GONE);
        mElectrode1.setChecked(false);
        mElectrode2.setChecked(false);
        mElectrode3.setChecked(false);
        mElectrode4.setChecked(false);
        mSpecifiedU.requestFocus();
        switch (object) {
            case OBJECT_1:
                mSpecifiedU.setText(String.format("%d", specifiedU));
                disableView(mSpecifiedU);
                mSpecifiedI.setText(String.format("%.0f", specifiedI));
                disableView(mSpecifiedI);
                mSpecifiedT.setText(String.format("%d", specifiedT));
                disableView(mSpecifiedT);
                mElectrodeTitle.setVisibility(View.VISIBLE);
                mElectrodes.setVisibility(View.VISIBLE);
                break;
            case OBJECT_2:
                mSpecifiedU.setText(String.format("%d", specifiedU));
                disableView(mSpecifiedU);
                mSpecifiedI.setText(String.format("%.2f", specifiedI));
                disableView(mSpecifiedI);
                mSpecifiedT.setText(String.format("%d", specifiedT));
                disableView(mSpecifiedT);
                mElectrodeTitle.setVisibility(View.VISIBLE);
                mElectrodes.setVisibility(View.VISIBLE);
                break;
            case OBJECT_3:
                mSpecifiedU.setText(String.format("%d", specifiedU));
                disableView(mSpecifiedU);
                mSpecifiedI.setText(String.format("%.0f", specifiedI));
                disableView(mSpecifiedI);
                mSpecifiedT.setText(String.format("%d", specifiedT));
                disableView(mSpecifiedT);
                mElectrodeTitle.setVisibility(View.VISIBLE);
                mElectrodes.setVisibility(View.VISIBLE);
                break;
            case OBJECT_4:
                mSpecifiedU.setText("");
                enableView(mSpecifiedU);
                mSpecifiedI.setText("-");
                disableView(mSpecifiedI);
                mSpecifiedT.setText("-");
                disableView(mSpecifiedT);
                break;
            case OBJECT_5:
                mSpecifiedU.setText("");
                enableView(mSpecifiedU);
                mSpecifiedI.setText("-");
                disableView(mSpecifiedI);
                mSpecifiedT.setText("-");
                disableView(mSpecifiedT);
                break;
            case OBJECT_6:
                mSpecifiedU.setText(String.format("%d", specifiedU));
                disableView(mSpecifiedU);
                mSpecifiedI.setText("-");
                disableView(mSpecifiedI);
                mSpecifiedT.setText(String.format("%d", specifiedT));
                disableView(mSpecifiedT);
                mElectrodeTitle.setVisibility(View.VISIBLE);
                mElectrodes.setVisibility(View.VISIBLE);
                break;
            case OBJECT_7:
                mSpecifiedU.setText(String.format("%d", specifiedU));
                disableView(mSpecifiedU);
                mSpecifiedI.setText("-");
                disableView(mSpecifiedI);
                mSpecifiedT.setText(String.format("%d", specifiedT));
                disableView(mSpecifiedT);
                mElectrodeTitle.setVisibility(View.VISIBLE);
                mElectrodes.setVisibility(View.VISIBLE);
                break;
            case OBJECT_8:
                mSpecifiedU.setText("");
                enableView(mSpecifiedU);
                mSpecifiedI.setText("-");
                disableView(mSpecifiedI);
                mSpecifiedT.setText(String.format("%d", specifiedT));
                disableView(mSpecifiedT);
                break;
            case OBJECT_9:
                mSpecifiedU.setText("");
                enableView(mSpecifiedU);
                mSpecifiedI.setText("-");
                disableView(mSpecifiedI);
                mSpecifiedT.setText("-");
                disableView(mSpecifiedT);
                break;
        }
    }

    @OnTextChanged(R.id.specified_u)
    public void onSpecifiedUChanged(CharSequence charSequenceU) {
        mMainPresenter.changeSpecifiedU(charSequenceU.toString());
    }

    @Override
    public void enableObjectEnter() {
        enableView(mObjectEnter);
    }

    @Override
    public void disableObjectEnter() {
        disableView(mObjectEnter);
    }

    @OnCheckedChanged({R.id.electrode1, R.id.electrode2, R.id.electrode3, R.id.electrode4})
    public void onCheckedChanged(CompoundButton view) {
        switch (view.getId()) {
            case R.id.electrode1:
                mMainPresenter.selectElectrode(1, view.isChecked());
                break;
            case R.id.electrode2:
                mMainPresenter.selectElectrode(2, view.isChecked());
                break;
            case R.id.electrode3:
                mMainPresenter.selectElectrode(3, view.isChecked());
                break;
            case R.id.electrode4:
                mMainPresenter.selectElectrode(4, view.isChecked());
                break;
        }
    }

    @Override
    public void disableCheckboxes(boolean checkbox1, boolean checkbox2, boolean checkbox3, boolean checkbox4) {
        if (checkbox1) {
            disableView(mElectrode1);
        } else {
            enableView(mElectrode1);
        }
        if (checkbox2) {
            disableView(mElectrode2);
        } else {
            enableView(mElectrode2);
        }
        if (checkbox3) {
            disableView(mElectrode3);
        } else {
            enableView(mElectrode3);
        }
        if (checkbox4) {
            disableView(mElectrode4);
        } else {
            enableView(mElectrode4);
        }
    }

    @Override
    public void disableObjectTab() {
        disableView(mSerialNumberEnter);
        disableView(mObjectsSelector);
        disableView(mSpecifiedU);
        disableView(mSpecifiedT);
        setViewAndChildrenEnabled(mElectrodes, false);
        disableView(mObjectEnter);
        mObjectCancel.setVisibility(View.VISIBLE);
        mObjectNext.setVisibility(View.VISIBLE);
    }

    @Override
    public void enableObjectTab(boolean needEnableSpecifiedT) {
        enableView(mSerialNumberEnter);
        enableView(mObjectsSelector);
        enableView(mSpecifiedU);
        setViewAndChildrenEnabled(mElectrodes, true);
        enableView(mObjectEnter);
        mObjectCancel.setVisibility(View.GONE);
        mObjectNext.setVisibility(View.GONE);
    }

    @Override
    public void checkEnabledU(String object) {
        switch (object) {
            case OBJECT_1:
            case OBJECT_2:
            case OBJECT_3:
            case OBJECT_6:
            case OBJECT_7:
                disableView(mSpecifiedU);
                break;
            case OBJECT_4:
            case OBJECT_5:
            case OBJECT_8:
            case OBJECT_9:
                enableView(mSpecifiedU);
                break;
        }
    }

    @Override
    public void changeTabToExperiments() {
        setViewAndChildrenVisibility(mTabExperiments, View.VISIBLE);
        mTabHost.setCurrentTab(EXPERIMENTS_TAB_INDEX);
    }

    @Override
    public void showSpecifiedParams(String object, boolean electrode1, boolean electrode2, boolean electrode3, boolean electrode4, int specifiedU, float specifiedI, int specifiedT) {
        if (object.equals(OBJECT_1) || object.equals(OBJECT_2) || object.equals(OBJECT_3) || object.equals(OBJECT_6) || object.equals(OBJECT_7)) {
            if (electrode1) {
                mTableExperiment1.setVisibility(View.VISIBLE);
            }
            if (electrode2) {
                mTableExperiment2.setVisibility(View.VISIBLE);
            }
            if (electrode3) {
                mTableExperiment3.setVisibility(View.VISIBLE);
            }
            if (electrode4) {
                mTableExperiment4.setVisibility(View.VISIBLE);
            }
        } else {
            mTableExperiment1.setVisibility(View.VISIBLE);
        }

        if (object.equals(OBJECT_4) || object.equals(OBJECT_5) || object.equals(OBJECT_9)) {
            mFixResult.setVisibility(View.VISIBLE);
        } else {
            mFixResult.setVisibility(View.GONE);
        }

        mParameter1_1.setText(String.format("%d", specifiedU));
        mParameter2_1.setText(String.format("%d", specifiedU));
        mParameter3_1.setText(String.format("%d", specifiedU));
        mParameter4_1.setText(String.format("%d", specifiedU));

        mParameter1_2.setText("");
        mParameter2_2.setText("");
        mParameter3_2.setText("");
        mParameter4_2.setText("");

        mParameter1_3.setText(String.format("%.1f", specifiedI));
        mParameter2_3.setText(String.format("%.1f", specifiedI));
        mParameter3_3.setText(String.format("%.1f", specifiedI));
        mParameter4_3.setText(String.format("%.1f", specifiedI));

        mParameter1_4.setText("");
        mParameter2_4.setText("");
        mParameter3_4.setText("");
        mParameter4_4.setText("");

        mParameter1_5.setText(String.format("%d", specifiedT));
        mParameter2_5.setText(String.format("%d", specifiedT));
        mParameter3_5.setText(String.format("%d", specifiedT));
        mParameter4_5.setText(String.format("%d", specifiedT));

        mParameter1_6.setText("");
        mParameter2_6.setText("");
        mParameter3_6.setText("");
        mParameter4_6.setText("");
    }

    @Override
    public void initializeSpecifiedParamsTables() {
        mTableExperiment1.setVisibility(View.GONE);
        mTableExperiment2.setVisibility(View.GONE);
        mTableExperiment3.setVisibility(View.GONE);
        mTableExperiment4.setVisibility(View.GONE);
    }

    private void initializeDateButton() {
        mSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = DatePickerFragment.newInstance(mSelectDate.getText().toString());
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
        mSelectDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayAdapter<Protocol> protocolArrayAdapter =
                        new ArrayAdapter<>(MainActivity.this,
                                R.layout.spinner_layout,
                                mMainModel.getProtocolsByDateFromDB(getStartDate(mSelectDate.getText().toString()), getEndDate(mSelectDate.getText().toString())));
                mProtocols.setAdapter(protocolArrayAdapter);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mSelectDate.setText(mFormat.format(System.currentTimeMillis()));
    }

    @Override
    public void update(Observable o, final Object values) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                int param = (int) ((Object[]) values)[0];
                Object value = ((Object[]) values)[1];
                switch (param) {
                    case REFRESH_LOG:
//                        mLog.setText(Log.getLines());
                        break;
                    case OBJECT_U_1:
                        mParameter1_2.setText(String.format("%.2f", (float) value));
                        break;
                    case OBJECT_U_2:
                        mParameter2_2.setText(String.format("%.2f", (float) value));
                        break;
                    case OBJECT_U_3:
                        mParameter3_2.setText(String.format("%.2f", (float) value));
                        break;
                    case OBJECT_U_4:
                        mParameter4_2.setText(String.format("%.2f", (float) value));
                        break;
                    case OBJECT_I_1:
                        mParameter1_4.setText(String.format("%.2f", (float) value));
                        break;
                    case OBJECT_I_2:
                        mParameter2_4.setText(String.format("%.2f", (float) value));
                        break;
                    case OBJECT_I_3:
                        mParameter3_4.setText(String.format("%.2f", (float) value));
                        break;
                    case OBJECT_I_4:
                        mParameter4_4.setText(String.format("%.2f", (float) value));
                        break;
                    case OBJECT_T_1:
                        mParameter1_5.setText(String.format("%d", (int) value));
                        break;
                    case OBJECT_T_2:
                        mParameter2_5.setText(String.format("%d", (int) value));
                        break;
                    case OBJECT_T_3:
                        mParameter3_5.setText(String.format("%d", (int) value));
                        break;
                    case OBJECT_T_4:
                        mParameter4_5.setText(String.format("%d", (int) value));
                        break;
                    case OBJECT_RESULT_1:
                        mParameter1_6.setText(String.format("%s", (((int) value) == 1) ? "Успешно" : "Неуспешно"));
                        break;
                    case OBJECT_RESULT_2:
                        mParameter2_6.setText(String.format("%s", (((int) value) == 1) ? "Успешно" : "Неуспешно"));
                        break;
                    case OBJECT_RESULT_3:
                        mParameter3_6.setText(String.format("%s", (((int) value) == 1) ? "Успешно" : "Неуспешно"));
                        break;
                    case OBJECT_RESULT_4:
                        mParameter4_6.setText(String.format("%s", (((int) value) == 1) ? "Успешно" : "Неуспешно"));
                        break;
                }
            }
        });
    }

    @Override
    public void changeTabToResult() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mTabHost.setCurrentTab(RESULT_TAB_INDEX);
            }
        });
    }

    @Override
    public void reportThatDevicesNotRespond() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(
                        MainActivity.this)
                        .setTitle("Внимание")
                        .setCancelable(false)
                        .setMessage("Одно или несколько устройств не отвечают, попробуйте переинициализировать стенд")
                        .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                mMainPresenter.objectCancel();
                            }
                        })
                        .create()
                        .show();
            }
        });
    }

    @Override
    public void showTheEndOfTestWindow(final int result) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                StringBuilder title = new StringBuilder();
                switch (result) {
                    case OK_RESULT:
                        title.append("Опыт закончен успешно");
                        break;
                    case BREAKDOWN_RESULT:
                        title.append("Произошёл пробой");
                        break;
                    case CONC_UP_RESULT:
                        title.append("Не удалось завершить испытание");
                        break;
                }
                AlertDialog dialog = new AlertDialog.Builder(
                        MainActivity.this)
                        .setTitle(title)
                        .setCancelable(false)
                        .setMessage("Сохранить результаты?")
                        .setPositiveButton("ДА", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                mMainPresenter.setNeedToSave(true);
                                mTabHost.setCurrentTab(PROTOCOL_TAB_INDEX);
                            }
                        })
                        .setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                mMainPresenter.objectCancel();
                                uncheckSerialNumberEnter();
                                mTabHost.setCurrentTab(OBJECT_TAB_INDEX);
                            }
                        })
                        .create();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                @SuppressWarnings("ConstantConditions") WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
                wmlp.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
                dialog.show();
            }
        });
    }

    private void changeTabToProtocol() {
        mMainPresenter.setNeedToSave(true);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mTabHost.setCurrentTab(PROTOCOL_TAB_INDEX);
            }
        });
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private static String date;

        public static DatePickerFragment newInstance(String date) {
            DatePickerFragment f = new DatePickerFragment();
            Bundle args = new Bundle();
            args.putString("date", date);
            f.setArguments(args);
            return f;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(getArguments().getString("date")));
            } catch (ParseException ignored) {
            }
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            date = dayOfMonth + "-" + ++month + "-" + year;
            ((MainActivity) getActivity()).mSelectDate.setText(date);
        }
    }

    @Override
    public void hideExperimentsViews() {
        setViewAndChildrenVisibility(mTabExperiments, View.GONE);
    }
}