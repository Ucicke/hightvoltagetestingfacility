package ru.avem.highvoltagetestingfacility.communication.devices.ammeter;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Observer;

import ru.avem.highvoltagetestingfacility.communication.devices.DeviceController;
import ru.avem.highvoltagetestingfacility.communication.protocol.modbus.ModbusController;

public class AmmeterController implements DeviceController {
    private static final short A_REGISTER = 7;

    private static final int NUM_OF_WORDS_IN_REGISTER = 1;
    private static final short NUM_OF_REGISTERS = 2 * NUM_OF_WORDS_IN_REGISTER;

    private AmmeterModel mModel;
    private byte mModbusAddress;
    private ModbusController mModbusController;
    private byte mAttempt = NUMBER_OF_ATTEMPTS;
    private boolean mNeedToReed;

    public AmmeterController(int modbusAddress, Observer observer, ModbusController controller, int id) {
        mModbusAddress = (byte) modbusAddress;
        mModel = new AmmeterModel(observer, id);
        mModbusController = controller;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusController.readInputRegisters(
                    mModbusAddress, A_REGISTER, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
                mModel.setI(inputBuffer.order(ByteOrder.LITTLE_ENDIAN).getFloat() * 1000);
            } else {
                read(args);
            }
        } else {
            mModel.setResponding(false);
            mModel.setI(0);
        }
    }

    @Override
    public void write(Object... args) {
    }

    @Override
    public void resetAttempts() {
        mAttempt = NUMBER_OF_ATTEMPTS;
    }

    @Override
    public boolean thereAreAttempts() {
        return mAttempt > 0;
    }

    @Override
    public boolean needToRead() {
        return mNeedToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        mNeedToReed = needToRead;
    }
}