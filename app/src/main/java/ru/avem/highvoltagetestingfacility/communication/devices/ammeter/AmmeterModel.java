package ru.avem.highvoltagetestingfacility.communication.devices.ammeter;

import java.util.Observable;
import java.util.Observer;

public class AmmeterModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int I_PARAM = 1;

    private int mId;

    AmmeterModel(Observer observer, int id) {
        addObserver(observer);
        mId = id;
    }

    public void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    void setI(float i) {
//        Log.i("DEBUG_TAG", "id: " + mId +" setI: " + i);
        notice(I_PARAM, i);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{mId, param, value});
    }
}