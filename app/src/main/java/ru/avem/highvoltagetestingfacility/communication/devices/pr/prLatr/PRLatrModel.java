package ru.avem.highvoltagetestingfacility.communication.devices.pr.prLatr;

import java.util.Observable;
import java.util.Observer;

import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.PR_LATR_ID;

public class PRLatrModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int ALARM_PARAM = 1;
    public static final int AUTO_PARAM = 2;
    public static final int UP_PARAM = 3;
    public static final int DOWN_PARAM = 4;
    public static final int CONC_UP_PARAM = 5;
    public static final int CONC_DOWN_PARAM = 6;
    public static final int TIMER_STATE_PARAM = 7;

    PRLatrModel(Observer observer) {
        addObserver(observer);
    }

    void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    void setAlarm(boolean alarm) {
        notice(ALARM_PARAM, alarm);
    }

    public void setAuto(boolean auto) {
        notice(AUTO_PARAM, auto);
    }

    public void setUp(boolean up) {
        notice(UP_PARAM, up);
    }

    void setDown(boolean down) {
        notice(DOWN_PARAM, down);
    }

    void setConcUp(boolean concUp) {
        notice(CONC_UP_PARAM, concUp);
    }

    void setConcDown(boolean concDown) {
        notice(CONC_DOWN_PARAM, concDown);
    }

    void setTimerState(boolean timerState) {
        notice(TIMER_STATE_PARAM, timerState);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{PR_LATR_ID, param, value});
    }
}