package ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k;

import java.nio.ByteBuffer;
import java.util.Observer;

import ru.avem.highvoltagetestingfacility.communication.devices.DeviceController;
import ru.avem.highvoltagetestingfacility.communication.protocol.modbus.ModbusController;

public class MUController implements DeviceController {
    private static final byte MODBUS_ADDRESS = 4;
    public static final short OUT_1 = 0;
    public static final short OUT_2 = 1;
    public static final short OUT_3 = 2;
    public static final short OUT_4 = 3;
    public static final short OUT_5 = 4;
    public static final short OUT_6 = 5;
    public static final short OUT_7 = 6;
    public static final short OUT_8 = 7;
    public static final short OUT_9 = 8;
    public static final short OUT_10 = 9;
    public static final short OUT_11 = 10;
    public static final short OUT_12 = 11;
    public static final short OUT_13 = 12;
    public static final short OUT_14 = 13;
    public static final short OUT_15 = 14;
    public static final short OUT_16 = 15;

    private static final int CONVERT_BUFFER_SIZE = 2;
    private static final int NUM_OF_WORDS_IN_REGISTER = 1;
    private static final short NUM_OF_REGISTERS = 3 * NUM_OF_WORDS_IN_REGISTER;

    private MUModel mModel;
    private ModbusController mModbusProtocol;
    private byte mAttempt = NUMBER_OF_ATTEMPTS;
    private boolean mNeedToReed;

    public MUController(Observer observer, ModbusController controller) {
        mModel = new MUModel(observer);
        mModbusProtocol = controller;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusProtocol.readInputRegisters(
                    MODBUS_ADDRESS, OUT_1, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
                mModel.setKM2(inputBuffer.getShort() == 1000);
                mModel.setKM3(inputBuffer.getShort() == 1000);
                mModel.setKM4(inputBuffer.getShort() == 1000);
            } else {
                read(args);
            }
        } else {
            mModel.setResponding(false);
            mModel.setKM2(false);
            mModel.setKM3(false);
            mModel.setKM4(false);
        }
    }

    @Override
    public void write(Object... args) {
        short register = (short) args[0];
        int numOfRegisters = (int) args[1];
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        ByteBuffer dataBuffer = ByteBuffer.allocate(2 * numOfRegisters);
        for (int i = 2; i < numOfRegisters + 2; i++) {
            dataBuffer.putShort((short) ((int) args[i]));
        }
        dataBuffer.flip();

        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusProtocol.writeMultipleHoldingRegisters(
                    MODBUS_ADDRESS, register, (short) numOfRegisters, dataBuffer, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
            } else {
                write(args);
            }
        } else {
            mModel.setResponding(false);
        }
    }

    @Override
    public void resetAttempts() {
        mAttempt = NUMBER_OF_ATTEMPTS;
    }

    @Override
    public boolean thereAreAttempts() {
        return mAttempt > 0;
    }

    @Override
    public boolean needToRead() {
        return mNeedToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        mNeedToReed = needToRead;
    }
}