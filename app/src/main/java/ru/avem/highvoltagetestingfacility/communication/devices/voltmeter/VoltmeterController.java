package ru.avem.highvoltagetestingfacility.communication.devices.voltmeter;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Observer;

import ru.avem.highvoltagetestingfacility.communication.devices.DeviceController;
import ru.avem.highvoltagetestingfacility.communication.protocol.modbus.ModbusController;

public class VoltmeterController implements DeviceController {
    private static final short U_REGISTER = 7;

    private static final int NUM_OF_WORDS_IN_REGISTER = 1;
    private static final short NUM_OF_REGISTERS = 2 * NUM_OF_WORDS_IN_REGISTER;

    private VoltmeterModel mModel;
    private byte mModbusAddress;
    private ModbusController mModbusController;
    private byte mAttempt = NUMBER_OF_ATTEMPTS;
    private boolean mNeedToReed;

    public VoltmeterController(int modbusAddress, Observer observer, ModbusController controller, int id) {
        mModbusAddress = (byte) modbusAddress;
        mModel = new VoltmeterModel(observer, id);
        mModbusController = controller;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusController.readInputRegisters(
                    mModbusAddress, U_REGISTER, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
                mModel.setU(inputBuffer.order(ByteOrder.LITTLE_ENDIAN).getFloat());
            } else {
                read(args);
            }
        } else {
            mModel.setResponding(false);
            mModel.setU(0);
        }
    }

    @Override
    public void write(Object... args) {
    }

    @Override
    public void resetAttempts() {
        mAttempt = NUMBER_OF_ATTEMPTS;
    }

    @Override
    public boolean thereAreAttempts() {
        return mAttempt > 0;
    }

    @Override
    public boolean needToRead() {
        return mNeedToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        mNeedToReed = needToRead;
    }
}