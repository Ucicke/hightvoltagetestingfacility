package ru.avem.highvoltagetestingfacility.communication.devices.pm130;

import java.util.Observable;
import java.util.Observer;

import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.PM130_ID;

public class PM130Model extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int V1_PARAM = 1;
    public static final int V2_PARAM = 2;
    public static final int V3_PARAM = 3;
    public static final int I1_PARAM = 4;
    public static final int I2_PARAM = 5;
    public static final int I3_PARAM = 6;

    PM130Model(Observer observer) {
        addObserver(observer);
    }

    void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    void setV1(float v1) {
        notice(V1_PARAM, v1);
    }

    void setV2(float v2) {
        notice(V2_PARAM, v2);
    }

    void setV3(float v3) {
        notice(V3_PARAM, v3);
    }

    void setI1(float i1) {
        notice(I1_PARAM, i1);
    }

    void setI2(float i2) {
        notice(I2_PARAM, i2);
    }

    void setI3(float i3) {
        notice(I3_PARAM, i3);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{PM130_ID, param, value});
    }
}