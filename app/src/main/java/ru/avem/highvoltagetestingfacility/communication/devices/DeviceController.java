package ru.avem.highvoltagetestingfacility.communication.devices;

public interface DeviceController {
    int INPUT_BUFFER_SIZE = 256;
    int NUMBER_OF_ATTEMPTS = 8;

    int BROADCAST_ID = 0;

    int PM130_ID = 1;
    int PR_CONTROL_ID = 2;
    int PR_LATR_ID = 3;

    int AMMETER_N2_ID = 4;
    int VOLTMETER_N3_ID = 5;
    int AMMETER_N6_ID = 6;
    int AMMETER_N7_ID = 7;
    int AMMETER_N8_ID = 8;
    int AMMETER_N9_ID = 9;
    int VOLTMETER_N10_ID = 10;
    int AMMETER_N11_ID = 11;

    int MU_ID = 12;

    void read(Object... args);

    void write(Object... args);

    void resetAttempts();

    boolean thereAreAttempts();

    boolean needToRead();

    void setNeedToRead(boolean needToRead);
}