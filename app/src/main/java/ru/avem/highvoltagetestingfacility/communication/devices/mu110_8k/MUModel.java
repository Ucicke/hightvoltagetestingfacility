package ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k;

import java.util.Observable;
import java.util.Observer;

import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.MU_ID;

public class MUModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int KM2 = 1;
    public static final int KM3 = 2;
    public static final int KM4 = 3;

    MUModel(Observer observer) {
        addObserver(observer);
    }

    void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    void setKM2(boolean km2) {
        notice(KM2, km2);
    }

    void setKM3(boolean km3) {
        notice(KM3, km3);
    }

    void setKM4(boolean km4) {
        notice(KM4, km4);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{MU_ID, param, value});
    }
}