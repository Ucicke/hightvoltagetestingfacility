package ru.avem.highvoltagetestingfacility.communication.devices.pm130;

import java.nio.ByteBuffer;
import java.util.Observer;

import ru.avem.highvoltagetestingfacility.communication.devices.DeviceController;
import ru.avem.highvoltagetestingfacility.communication.protocol.modbus.ModbusController;

public class PM130Controller implements DeviceController {
    private static final byte MODBUS_ADDRESS = 14;
    private static final short V1_REGISTER = 13312;

    private static final int CONVERT_BUFFER_SIZE = 4;
    private static final float U_DIVIDER = 10.f;
    private static final int U_MULTIPLIER = 1;
    private static final float I_DIVIDER = 100.f;
    private static final int I_MULTIPLIER = 1000;
    private static final int NUM_OF_WORDS_IN_REGISTER = 2;
    private static final short NUM_OF_REGISTERS = 6 * NUM_OF_WORDS_IN_REGISTER;

    private PM130Model mModel;
    private ModbusController mModbusController;
    private byte mAttempt = NUMBER_OF_ATTEMPTS;
    private boolean mNeedToReed;

    public PM130Controller(Observer observer, ModbusController modbusController) {
        mModel = new PM130Model(observer);
        mModbusController = modbusController;
    }

    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusController.readInputRegisters(
                    MODBUS_ADDRESS, V1_REGISTER, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
                mModel.setV1(convertUINTtoINT(inputBuffer.getInt()) * U_MULTIPLIER  / U_DIVIDER);
                mModel.setV2(convertUINTtoINT(inputBuffer.getInt()) * U_MULTIPLIER  / U_DIVIDER);
                mModel.setV3(convertUINTtoINT(inputBuffer.getInt()) * U_MULTIPLIER  / U_DIVIDER);
                mModel.setI1(convertUINTtoINT(inputBuffer.getInt()) * I_MULTIPLIER / I_DIVIDER);
                mModel.setI2(convertUINTtoINT(inputBuffer.getInt()) * I_MULTIPLIER / I_DIVIDER);
                mModel.setI3(convertUINTtoINT(inputBuffer.getInt()) * I_MULTIPLIER / I_DIVIDER);
            } else {
                read(args);
            }
        } else {
            mModel.setResponding(false);
            mModel.setV1(0);
            mModel.setV2(0);
            mModel.setV3(0);
            mModel.setI1(0);
            mModel.setI2(0);
            mModel.setI3(0);
        }
    }

    @Override
    public void write(Object... args) {
    }

    @Override
    public void resetAttempts() {
        mAttempt = NUMBER_OF_ATTEMPTS;
    }

    @Override
    public boolean thereAreAttempts() {
        return mAttempt > 0;
    }

    private long convertUINTtoINT(int i) {
        ByteBuffer convertBuffer = ByteBuffer.allocate(CONVERT_BUFFER_SIZE);
        convertBuffer.clear();
        convertBuffer.putInt(i);
        convertBuffer.flip();
        short rightSide = convertBuffer.getShort();
        short leftSide = convertBuffer.getShort();
        convertBuffer.clear();
        convertBuffer.putShort(leftSide);
        convertBuffer.putShort(rightSide);
        convertBuffer.flip();
        int preparedInt = convertBuffer.getInt();
        return (long) preparedInt & 0xFFFFFFFFL;
    }

    @Override
    public boolean needToRead() {
        return mNeedToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        mNeedToReed = needToRead;
    }
}