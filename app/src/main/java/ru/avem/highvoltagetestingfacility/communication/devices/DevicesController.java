package ru.avem.highvoltagetestingfacility.communication.devices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

import ru.avem.highvoltagetestingfacility.communication.devices.ammeter.AmmeterController;
import ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k.MUController;
import ru.avem.highvoltagetestingfacility.communication.devices.pm130.PM130Controller;
import ru.avem.highvoltagetestingfacility.communication.devices.pr.prControl.PRControlController;
import ru.avem.highvoltagetestingfacility.communication.devices.pr.prLatr.PRLatrController;
import ru.avem.highvoltagetestingfacility.communication.devices.voltmeter.VoltmeterController;
import ru.avem.highvoltagetestingfacility.communication.protocol.modbus.ModbusController;
import ru.avem.highvoltagetestingfacility.communication.protocol.modbus.RTUController;
import ru.avem.highvoltagetestingfacility.communication.serial.SerialConnection;
import ru.avem.highvoltagetestingfacility.utils.Logger;
import ru.avem.highvoltagetestingfacility.view.OnBroadcastCallback;

import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N11_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N2_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N6_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N7_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N8_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N9_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.BROADCAST_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.VOLTMETER_N3_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k.MUController.OUT_1;
import static ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k.MUController.OUT_10;
import static ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k.MUController.OUT_13;
import static ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k.MUController.OUT_2;
import static ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k.MUController.OUT_3;
import static ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k.MUController.OUT_9;
import static ru.avem.highvoltagetestingfacility.communication.devices.pr.prControl.PRControlController.HOL_REGISTER;
import static ru.avem.highvoltagetestingfacility.communication.devices.pr.prControl.PRControlController.OBJ_REGISTER;
import static ru.avem.highvoltagetestingfacility.communication.devices.pr.prControl.PRControlController.RES_REGISTER;
import static ru.avem.highvoltagetestingfacility.communication.devices.pr.prLatr.PRLatrController.DO_UP_REGISTER;
import static ru.avem.highvoltagetestingfacility.communication.devices.pr.prLatr.PRLatrController.RESET_DOG_REGISTER;
import static ru.avem.highvoltagetestingfacility.communication.devices.pr.prLatr.PRLatrController.RESET_TIME_REGISTER;
import static ru.avem.highvoltagetestingfacility.communication.devices.pr.prLatr.PRLatrController.SWITCH_TRANSF_KM_REGISTER;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.A_P11;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.A_P2;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.A_P6_P9;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.V_P3;
import static ru.avem.highvoltagetestingfacility.presenter.MainPresenter.V_PM130;

public class DevicesController extends Observable implements Runnable {
    public static final String ACTION_USB_PERMISSION =
            "ru.avem.highvoltagetestingfacility.USB_PERMISSION";
    private static final String ACTION_USB_ATTACHED =
            "android.hardware.usb.action.USB_DEVICE_ATTACHED";
    private static final String ACTION_USB_DETACHED =
            "android.hardware.usb.action.USB_DEVICE_DETACHED";

    private static final int WRITE_TIMEOUT = 25;
    private static final int READ_TIMEOUT = 25;

    private static final String RS485_DEVICE_NAME = "CP2103 USB to RS-485";
    private static final int BAUD_RATE = 38400;

    private SerialConnection mRS485Connection;

    private List<DeviceController> mDevicesControllers = new ArrayList<>();

    private DeviceController mPM130Controller;//V3 для частичного при КМ2, для полного при КМ3/КМ4

    private DeviceController mPRControlController;
    private DeviceController mPRLATRController;

    private DeviceController mAmmeterControllerN2;//для измерения при КМ2
    private DeviceController mVoltmeterControllerN3;//для измерения при КМ2
    private DeviceController mAmmeterControllerN6;//1 элекрод
    private DeviceController mAmmeterControllerN7;//2 элекрод
    private DeviceController mAmmeterControllerN8;//3 элекрод
    private DeviceController mAmmeterControllerN9;//4 элекрод
//    private DeviceController mVoltmeterControllerN10;//для измерения при КМ3/КМ4
    private DeviceController mAmmeterControllerN11;//для измерения при КМ3/КМ4

    private DeviceController mMUController;//для комутации

    private boolean mLastOne;
    private long mLastTime;

    private Observer mObserver;

    public DevicesController(final Context context, Observer observer, OnBroadcastCallback onBroadcastCallback) {
        mObserver = observer;
        addObserver(mObserver);
        mRS485Connection = new SerialConnection(context, RS485_DEVICE_NAME, BAUD_RATE,
                UsbSerialPort.DATABITS_8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE,
                WRITE_TIMEOUT, READ_TIMEOUT);
        ModbusController modbusController = new RTUController(mRS485Connection);

        mPM130Controller = new PM130Controller(observer, modbusController);
        mDevicesControllers.add(mPM130Controller);

        mPRControlController = new PRControlController(observer, modbusController);
        mDevicesControllers.add(mPRControlController);
        mPRLATRController = new PRLatrController(observer, modbusController);
        mDevicesControllers.add(mPRLATRController);

        mAmmeterControllerN2 = new AmmeterController(2, observer, modbusController, AMMETER_N2_ID);
        mDevicesControllers.add(mAmmeterControllerN2);
        mVoltmeterControllerN3 = new VoltmeterController(3, observer, modbusController, VOLTMETER_N3_ID);
        mDevicesControllers.add(mVoltmeterControllerN3);
        mAmmeterControllerN6 = new AmmeterController(6, observer, modbusController, AMMETER_N6_ID);
        mDevicesControllers.add(mAmmeterControllerN6);
        mAmmeterControllerN7 = new AmmeterController(7, observer, modbusController, AMMETER_N7_ID);
        mDevicesControllers.add(mAmmeterControllerN7);
        mAmmeterControllerN8 = new AmmeterController(8, observer, modbusController, AMMETER_N8_ID);
        mDevicesControllers.add(mAmmeterControllerN8);
        mAmmeterControllerN9 = new AmmeterController(9, observer, modbusController, AMMETER_N9_ID);
        mDevicesControllers.add(mAmmeterControllerN9);
//        mVoltmeterControllerN10 = new VoltmeterController(10, observer, modbusController, VOLTMETER_N10_ID);
//        mDevicesControllers.add(mVoltmeterControllerN10);
        mAmmeterControllerN11 = new AmmeterController(11, observer, modbusController, AMMETER_N11_ID);
        mDevicesControllers.add(mAmmeterControllerN11);

        mMUController = new MUController(observer, modbusController);
        mDevicesControllers.add(mMUController);

        Thread continuousReadingThread = new Thread(this);
        continuousReadingThread.start();

        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(ACTION_USB_ATTACHED);
        filter.addAction(ACTION_USB_DETACHED);
        final BroadcastReceiver usbReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (ACTION_USB_PERMISSION.equals(action)) {
                    synchronized (this) {
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            if (device != null) {
                                if (Objects.equals(device.getProductName(), RS485_DEVICE_NAME)) {
                                    resetDevicesAttempts();
                                }
//                                } else if (isDeviceFlashMassStorage(device)) {
//                                    Logging.saveFileOnFlashMassStorage(context, mModel.getProtocolForInteraction());
//                                }
                            }
                        } else {
                            if (device != null) {
                                if (Objects.equals(device.getProductName(), RS485_DEVICE_NAME)) {
                                    connectMainBus();
                                }
                            }
                        }
                    }
                } else if (ACTION_USB_DETACHED.equals(action) || ACTION_USB_ATTACHED.equals(action)) {
                    synchronized (this) {
                        disconnectMainBus();
                        connectMainBus();
//                        if (ACTION_USB_ATTACHED.equals(action) && isDeviceFlashMassStorage(device)) {
//                            synchronized (this) {
//                                UsbManager usbManager = (UsbManager) context.getSystemService(USB_SERVICE);
//                                PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
//                                if (usbManager != null) {
//                                    usbManager.requestPermission(device, pi);
//                                }
//                            }
//                        }
                    }
                }
            }
        };
        context.registerReceiver(usbReceiver, filter);
        onBroadcastCallback.onBroadcastUsbReceiver(usbReceiver);
    }

    @Override
    public void run() {
        while (true) {
            for (DeviceController deviceController : mDevicesControllers) {
                if (deviceController.needToRead() && deviceController.thereAreAttempts()) {
                    deviceController.read();
                    if (deviceController instanceof PRLatrController) {
                        resetDog();
                    }
                }
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long currentTime = System.currentTimeMillis();
            if (currentTime > (mLastTime + 1000)) {
                mLastTime = currentTime;
                setChanged();
                notifyObservers(new Object[]{BROADCAST_ID, 0, 0});
            }

        }
    }

    private void resetDog() {
        if (mLastOne) {
            mPRLATRController.write(RESET_DOG_REGISTER, 1, 0);
            mPRControlController.write(HOL_REGISTER, 1, 0);
            mLastOne = false;
        } else {
            mPRLATRController.write(RESET_DOG_REGISTER, 1, 1);
            mPRControlController.write(HOL_REGISTER, 1, 1);
            mLastOne = true;
        }
    }

    public void resetTimer() {
        mLastOne = true;
        mPRLATRController.write(RESET_DOG_REGISTER, 1, 0);
        mPRLATRController.write(RESET_DOG_REGISTER, 1, 1);
        mPRLATRController.write(RESET_TIME_REGISTER, 1, 1);
        mPRLATRController.write(RESET_TIME_REGISTER, 1, 0);

        mPRControlController.write(HOL_REGISTER, 1, 0);
        mPRControlController.write(HOL_REGISTER, 1, 1);
        mPRControlController.write(HOL_REGISTER, 1, 10);
        mPRControlController.write(HOL_REGISTER, 1, 0);
    }

    public void initMainGroupDevices() {
        connectMainBus();
        mPM130Controller.setNeedToRead(true);
        mPM130Controller.resetAttempts();
        mPRControlController.setNeedToRead(true);
        mPRControlController.resetAttempts();
        mPRLATRController.setNeedToRead(true);
        mPRLATRController.resetAttempts();
        mMUController.setNeedToRead(true);
        mMUController.resetAttempts();
    }

    public void initVoltmeterDevice(String voltmeterDeviceType) {
        switch (voltmeterDeviceType) {
            case V_P3:
                mVoltmeterControllerN3.setNeedToRead(true);
                mVoltmeterControllerN3.resetAttempts();
                break;
            case V_PM130:
                break;
        }
    }

    public void initAmmetersGroupDevices(String isOne,
                                          boolean electrode1, boolean electrode2,
                                          boolean electrode3, boolean electrode4) {
        switch (isOne) {
            case A_P2:
                mAmmeterControllerN2.setNeedToRead(true);
                mAmmeterControllerN2.resetAttempts();
                break;
            case A_P11:
                mAmmeterControllerN11.setNeedToRead(true);
                mAmmeterControllerN11.resetAttempts();
                break;
            case A_P6_P9:
                if (electrode1) {
                    mAmmeterControllerN6.setNeedToRead(true);
                    mAmmeterControllerN6.resetAttempts();
                }
                if (electrode2) {
                    mAmmeterControllerN7.setNeedToRead(true);
                    mAmmeterControllerN7.resetAttempts();
                }
                if (electrode3) {
                    mAmmeterControllerN8.setNeedToRead(true);
                    mAmmeterControllerN8.resetAttempts();
                }
                if (electrode4) {
                    mAmmeterControllerN9.setNeedToRead(true);
                    mAmmeterControllerN9.resetAttempts();
                }
                break;
        }
    }

    public void diversifyDevices() {
        for (DeviceController devicesController : mDevicesControllers) {
            devicesController.setNeedToRead(false);
        }
    }

    public void connectMainBus() {
        Logger.withTag("DEBUG_TAG").log("connectMainBus");
        if (!mRS485Connection.isInitiated()) {
            Logger.withTag("DEBUG_TAG").log("!isInitiated");
            try {
                mRS485Connection.initSerialPort();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void resetDevicesAttempts() {
        mPM130Controller.resetAttempts();
        mPRControlController.resetAttempts();
        mPRLATRController.resetAttempts();
        mAmmeterControllerN2.resetAttempts();
        mVoltmeterControllerN3.resetAttempts();
        mAmmeterControllerN6.resetAttempts();
        mAmmeterControllerN7.resetAttempts();
        mAmmeterControllerN8.resetAttempts();
        mAmmeterControllerN9.resetAttempts();
//        mVoltmeterControllerN10.resetAttempts();
        mAmmeterControllerN11.resetAttempts();
        mMUController.resetAttempts();
    }

    public void disconnectMainBus() {
        mRS485Connection.closeSerialPort();
    }

    public void connectPM130() {
        checkRS485Connection();
        mPM130Controller.resetAttempts();
    }

    public void connectPRControl() {
//        checkRS485Connection();
        mPRControlController.resetAttempts();
    }

    public void connectPRLATR() {
        checkRS485Connection();
        mPRLATRController.resetAttempts();
    }

    public void connectAmmeterN2() {
        checkRS485Connection();
        mAmmeterControllerN2.resetAttempts();
    }

    public void connectVoltmeterN3() {
        checkRS485Connection();
        mVoltmeterControllerN3.resetAttempts();
    }

    public void connectAmmeterN6() {
        checkRS485Connection();
        mAmmeterControllerN6.resetAttempts();
    }

    public void connectAmmeterN7() {
        checkRS485Connection();
        mAmmeterControllerN7.resetAttempts();
    }

    public void connectAmmeterN8() {
        checkRS485Connection();
        mAmmeterControllerN8.resetAttempts();
    }

    public void connectAmmeterN9() {
        checkRS485Connection();
        mAmmeterControllerN9.resetAttempts();
    }

//    public void connectVoltmeterN10() {
//        checkRS485Connection();
//        mVoltmeterControllerN10.resetAttempts();
//    }

    public void connectAmmeterN11() {
        checkRS485Connection();
        mAmmeterControllerN11.resetAttempts();
    }

    private void checkRS485Connection() {
        if (!mRS485Connection.isInitiated()) {
            try {
                mRS485Connection.initSerialPort();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void startWithOnTransfKmLATR() {
        mPRLATRController.write(SWITCH_TRANSF_KM_REGISTER, 3, 1, 1, 0);
    }

    public void offTransfKmLATR() {
        mPRLATRController.write(SWITCH_TRANSF_KM_REGISTER, 1, 0);
    }

    public void stopWithOffTransfKmLATR() {
        mPRLATRController.write(SWITCH_TRANSF_KM_REGISTER, 3, 0, 0, 0);
    }

    public void startUpLATR() {
        mPRLATRController.write(DO_UP_REGISTER, 2, 1, 0);
    }

    public void startDownLATR() {
        mPRLATRController.write(DO_UP_REGISTER, 2, 0, 1);
    }

    public void stopLATR() {
        mPRLATRController.write(DO_UP_REGISTER, 2, 0, 0);
    }


    public void d() {
        connectMainBus();
        for (DeviceController devicesController : mDevicesControllers) {
            devicesController.resetAttempts();
            devicesController.read();
        }
//        Log.i("StatusActivity", "setFirst: ");
//        for (int i = 0; i < 3; i++) {
//            mMUController.write((short)(OUT_1 + i), 1, 1000);
//        }
//        for (int i = 0; i < 3; i++) {
//            mMUController.write((short)(OUT_1 + i), 1, 0);
//        }
    }

    public void controlToStart() {
        mPRControlController.write(RES_REGISTER, 1, 1);
    }

    public void controlToEnd() {
        mPRControlController.write(RES_REGISTER, 1, 0);
    }

    public void onKM4() {
        mMUController.write(OUT_3, 1, 1000);
    }

    public void offKM4() {
        mMUController.write(OUT_3, 1, 0);
    }

    public void onKM2() {
        mMUController.write(OUT_1, 1, 1000);
    }

    public void offKM2() {
        mMUController.write(OUT_1, 1, 0);
    }

    public void onKM3() {
        mMUController.write(OUT_2, 1, 1000);
    }

    public void offKM3() {
        mMUController.write(OUT_2, 1, 0);
    }

    public void setObj(boolean electrode1, boolean electrode2, boolean electrode3, boolean electrode4) {
        int obj = 0;
        if (electrode1) {
            obj |= 16;
        }
        if (electrode2) {
            obj |= 32;
        }
        if (electrode3) {
            obj |= 64;
        }
        if (electrode4) {
            obj |= 128;
        }
        mPRControlController.write(OBJ_REGISTER, 1, obj);
    }

    public void onShortCircuit() {
        mMUController.write(OUT_13, 1, 1000);
    }

    public void offShortCircuit() {
        mMUController.write(OUT_13, 1, 0);
    }

    public void onDoorLocking() {
        mMUController.write(OUT_9, 1, 1000);
    }

    public void onCarriageLocking() {
        mMUController.write(OUT_10, 1, 1000);
    }

    public void offCarriageLocking() {
        mMUController.write(OUT_10, 1, 0);
    }

    public void offDoorLocking() {
        mMUController.write(OUT_9, 1, 0);
    }
}