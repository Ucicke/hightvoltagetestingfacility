package ru.avem.highvoltagetestingfacility.communication.devices.pr.prControl;

import java.util.Observable;
import java.util.Observer;

import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.PR_CONTROL_ID;

public class PRControlModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int KM1_PARAM = 1;
    public static final int DOOR_S_PARAM = 2;
    public static final int DOOR_Z_PARAM = 3;
    public static final int OBJ_1_PARAM = 4;
    public static final int OBJ_2_PARAM = 5;
    public static final int OBJ_3_PARAM = 6;
    public static final int OBJ_4_PARAM = 7;
    public static final int KM2_PARAM = 8;
    public static final int KM3_PARAM = 9;
    public static final int KM4_PARAM = 10;
    public static final int Q1_PARAM = 11;
    public static final int SC_PARAM = 12;

    PRControlModel(Observer observer) {
        addObserver(observer);
    }

    void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    void setSta(short sta) {
        notice(KM1_PARAM, (sta & 0b1) == 0);
        notice(DOOR_S_PARAM, (sta & 0b10) == 0);
//        notice(PARAM, (sta & 0b100) > 0);
        notice(DOOR_Z_PARAM, (sta & 0b1000) == 0);
        notice(OBJ_1_PARAM, (sta & 0b10000) == 0);
        notice(OBJ_2_PARAM, (sta & 0b100000) == 0);
        notice(OBJ_3_PARAM, (sta & 0b1000000) == 0);
        notice(OBJ_4_PARAM, (sta & 0b10000000) == 0);
    }

    public void setStatKm(short sTatKm) {
        notice(KM2_PARAM, (sTatKm & 0b1) > 0);
        notice(KM3_PARAM, (sTatKm & 0b10) > 0);
        notice(KM4_PARAM, (sTatKm & 0b100) > 0);
        notice(Q1_PARAM, (sTatKm & 0b1000) > 0);
        notice(SC_PARAM, (sTatKm & 0b10000) == 0);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{PR_CONTROL_ID, param, value});
    }
}