package ru.avem.highvoltagetestingfacility.communication.devices.voltmeter;

import java.util.Observable;
import java.util.Observer;

public class VoltmeterModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int U_PARAM = 1;

    private int mId;

    VoltmeterModel(Observer observer, int id) {
        addObserver(observer);
        mId = id;
    }

    public void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    public void setU(float u) {
        notice(U_PARAM, u);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{mId, param, value});
    }
}