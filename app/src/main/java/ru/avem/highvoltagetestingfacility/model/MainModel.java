package ru.avem.highvoltagetestingfacility.model;

import android.content.Context;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import ru.avem.highvoltagetestingfacility.communication.devices.ammeter.AmmeterModel;
import ru.avem.highvoltagetestingfacility.communication.devices.mu110_8k.MUModel;
import ru.avem.highvoltagetestingfacility.communication.devices.pm130.PM130Model;
import ru.avem.highvoltagetestingfacility.communication.devices.pr.prControl.PRControlModel;
import ru.avem.highvoltagetestingfacility.communication.devices.pr.prLatr.PRLatrModel;
import ru.avem.highvoltagetestingfacility.communication.devices.voltmeter.VoltmeterModel;
import ru.avem.highvoltagetestingfacility.database.model.DatabaseAdapter;
import ru.avem.highvoltagetestingfacility.database.model.Protocol;
import ru.avem.highvoltagetestingfacility.utils.Logger;

import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N11_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N2_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N6_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N7_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N8_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.AMMETER_N9_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.BROADCAST_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.MU_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.PM130_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.PR_CONTROL_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.PR_LATR_ID;
import static ru.avem.highvoltagetestingfacility.communication.devices.DeviceController.VOLTMETER_N3_ID;

public class MainModel extends Observable implements Observer {
    public static final int REFRESH_LOG = 100;

    private DatabaseAdapter mDatabaseAdapter;

    private boolean mPM130Responding;
    private float mPM130V2;
    private float mPM130V3;

    private boolean mPRControlResponding;
    private boolean mKM1State;
    private boolean mObj1State;
    private boolean mObj2State;
    private boolean mObj3State;
    private boolean mObj4State;
    private boolean mKm2State;
    private boolean mKm3State;
    private boolean mKm4State;
    private boolean mQ1State;

    private boolean mPRLatrResponding;
    private boolean mOnlineAlarm;
    private boolean mOnlineConcUp;
    private boolean mOnlineConcDown;

    private boolean mAmmeterN2Responding;
    private float mAmmeterN2I;

    private boolean mVoltmeterN3Responding;
    private float mVoltmeterN3U;

    private boolean mAmmeterN6Responding;
    private float mAmmeterN6I;

    private boolean mAmmeterN7Responding;
    private float mAmmeterN7I;

    private boolean mAmmeterN8Responding;
    private float mAmmeterN8I;

    private boolean mAmmeterN9Responding;
    private float mAmmeterN9I;

//    private boolean mVoltmeterN10Responding;
//    private float mVoltmeterN10U;

    private boolean mAmmeterN11Responding;
    private float mAmmeterN11I;

    private boolean mMUResponding;
    private boolean mSCState;

    public MainModel(Observer observer) {
        addObserver(observer);
        mDatabaseAdapter = new DatabaseAdapter((Context) observer);
    }

    public List<Protocol> getAllProtocolsFromDB() {
        List<Protocol> protocols;
        mDatabaseAdapter.open();
        protocols = mDatabaseAdapter.getProtocols();
        mDatabaseAdapter.close();
        return protocols;
    }

    public List<Protocol> getProtocolsByDateFromDB(long startDate, long endDate) {
        List<Protocol> protocols;
        mDatabaseAdapter.open();
        protocols = mDatabaseAdapter.getProtocolsByDate(startDate, endDate);
        mDatabaseAdapter.close();
        return protocols;
    }

    public void saveProtocolToDB(Protocol protocol) {
        mDatabaseAdapter.open();
        if (protocol.getId() > 0) {
            mDatabaseAdapter.updateProtocol(protocol);
        } else {
            protocol.setId(mDatabaseAdapter.insertProtocol(protocol));
        }
        mDatabaseAdapter.close();
    }

    @Override
    public void update(Observable o, Object values) {
        int modelId = (int) (((Object[]) values)[0]);
        int param = (int) (((Object[]) values)[1]);
        Object value = (((Object[]) values)[2]);

        switch (modelId) {
            case BROADCAST_ID:
                notice(REFRESH_LOG, 0);
                break;
            case PM130_ID:
                switch (param) {
                    case PM130Model.RESPONDING_PARAM:
                        setPM130Responding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("V_PM130 " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case PM130Model.V1_PARAM:
                        break;
                    case PM130Model.V2_PARAM:
                        float v2 = (float) value;
                        if (isKm2State()) {
                            v2 *= 500;
                        } else if (isKm3State()) {
                            v2 *= 14.177;
                        }
                        setPM130V2(v2);
                        Logger.withTag("DEVICES_TAG").log("V_PM130 V2=" + v2);
                        break;
                    case PM130Model.V3_PARAM:
                        float v3 = (float) value;
                        if (isKm2State()) {
                            v3 *= 500;
                        } else if (isKm3State()) {
                            v3 *= 14.177;
                        }
                        setPM130V3(v3);
                        Logger.withTag("DEVICES_TAG").log("V_PM130 V3=" + v3);
                        break;
                    case PM130Model.I1_PARAM:
                        break;
                    case PM130Model.I2_PARAM:
                        break;
                    case PM130Model.I3_PARAM:
                        break;
                }
                break;
            case PR_CONTROL_ID:
                switch (param) {
                    case PRControlModel.RESPONDING_PARAM:
                        setPRControlResponding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("ПРка контрольная " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case PRControlModel.KM1_PARAM:
                        setKM1State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("KM1 " + ((boolean) value ? "ON" : "OFF"));
                        break;
                    case PRControlModel.DOOR_S_PARAM:
                        Logger.withTag("DEVICES_TAG").log("Дверь шкафа " + ((boolean) value ? "ОК" : "BAD"));
                        break;
                    case PRControlModel.DOOR_Z_PARAM:
                        Logger.withTag("DEVICES_TAG").log("Дверь зоны " + ((boolean) value ? "ОК" : "BAD"));
                        break;
                    case PRControlModel.OBJ_1_PARAM:
                        setObj1State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние объекта 1 " + ((boolean) value ? "ОК" : "BAD"));
                        break;
                    case PRControlModel.OBJ_2_PARAM:
                        setObj2State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние объекта 2 " + ((boolean) value ? "ОК" : "BAD"));
                        break;
                    case PRControlModel.OBJ_3_PARAM:
                        setObj3State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние объекта 3 " + ((boolean) value ? "ОК" : "BAD"));
                        break;
                    case PRControlModel.OBJ_4_PARAM:
                        setObj4State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние объекта 4 " + ((boolean) value ? "ОК" : "BAD"));
                        break;
                    case PRControlModel.KM2_PARAM:
                        setKm2State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние КМ2 " + ((boolean) value ? "ON" : "OFF"));
                        break;
                    case PRControlModel.KM3_PARAM:
                        setKm3State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние КМ3 " + ((boolean) value ? "ON" : "OFF"));
                        break;
                    case PRControlModel.KM4_PARAM:
                        setKm4State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние КМ4 " + ((boolean) value ? "ON" : "OFF"));
                        break;
                    case PRControlModel.Q1_PARAM:
                        setQ1State((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние Q1 " + ((boolean) value ? "ОК" : "BAD"));
                        break;
                    case PRControlModel.SC_PARAM:
                        setSCState((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние короткозамыкателя " + ((boolean) value ? "ОК" : "BAD"));
                        break;
                }
                break;
            case PR_LATR_ID:
                switch (param) {
                    case PRLatrModel.RESPONDING_PARAM:
                        setPRLatrResponding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("ПРка ЛАТРа " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case PRLatrModel.ALARM_PARAM:
                        setOnlineAlarm((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Состояние ПРОБОЯ " + ((boolean) value ? "ДА" : "НЕТ"));
                        break;
                    case PRLatrModel.AUTO_PARAM:
                        break;
                    case PRLatrModel.UP_PARAM:
                        break;
                    case PRLatrModel.DOWN_PARAM:
                        break;
                    case PRLatrModel.CONC_UP_PARAM:
                        setOnlineConcUp((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Верхний концевик " + ((boolean) value ? "ДА" : "НЕТ"));
                        break;
                    case PRLatrModel.CONC_DOWN_PARAM:
                        setOnlineConcDown((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("Нижний концевик " + ((boolean) value ? "ДА" : "НЕТ"));
                        break;
                }
                break;
            case AMMETER_N2_ID:
                switch (param) {
                    case AmmeterModel.RESPONDING_PARAM:
                        setAmmeterN2Responding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("N2 " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case AmmeterModel.I_PARAM:
                        setAmmeterN2I((float) value);
                        Logger.withTag("DEVICES_TAG").log("N2 I=" + ((float) value));
                        break;
                }
                break;
            case VOLTMETER_N3_ID:
                switch (param) {
                    case VoltmeterModel.RESPONDING_PARAM:
                        setVoltmeterN3Responding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("N3 " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case VoltmeterModel.U_PARAM:
                        setVoltmeterN3U(((float) value) * 1000);
                        Logger.withTag("DEVICES_TAG").log("N3 U=" + (((float) value) * 1000));
                        break;
                }
                break;
            case AMMETER_N6_ID:
                switch (param) {
                    case AmmeterModel.RESPONDING_PARAM:
                        setAmmeterN6Responding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("N6 " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case AmmeterModel.I_PARAM:
                        setAmmeterN6I((float) value);
                        Logger.withTag("DEVICES_TAG").log("N6 I=" + ((float) value));
                        break;
                }
                break;
            case AMMETER_N7_ID:
                switch (param) {
                    case AmmeterModel.RESPONDING_PARAM:
                        setAmmeterN7Responding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("N7 " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case AmmeterModel.I_PARAM:
                        setAmmeterN7I((float) value);
                        Logger.withTag("DEVICES_TAG").log("N7 I=" + ((float) value));
                        break;
                }
                break;
            case AMMETER_N8_ID:
                switch (param) {
                    case AmmeterModel.RESPONDING_PARAM:
                        setAmmeterN8Responding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("N8 " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case AmmeterModel.I_PARAM:
                        setAmmeterN8I((float) value);
                        Logger.withTag("DEVICES_TAG").log("N8 I=" + ((float) value));
                        break;
                }
                break;
            case AMMETER_N9_ID:
                switch (param) {
                    case AmmeterModel.RESPONDING_PARAM:
                        setAmmeterN9Responding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("N9 " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case AmmeterModel.I_PARAM:
                        setAmmeterN9I((float) value);
                        Logger.withTag("DEVICES_TAG").log("N9 I=" + ((float) value));
                        break;
                }
                break;
//            case VOLTMETER_N10_ID:
//                switch (param) {
//                    case VoltmeterModel.RESPONDING_PARAM:
//                        setVoltmeterN10Responding((boolean) value);
//                        Logger.withTag("DEVICES_TAG").log("N10 " + ((boolean) value ? "" : "не ") + "отвечает");
//                        break;
//                    case VoltmeterModel.U_PARAM:
//                        setVoltmeterN10U((float) value);
//                        Logger.withTag("DEVICES_TAG").log("N10 U=" + ((float) value));
//                        break;
//                }
//                break;
            case AMMETER_N11_ID:
                switch (param) {
                    case AmmeterModel.RESPONDING_PARAM:
                        setAmmeterN11Responding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("N11 " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                    case AmmeterModel.I_PARAM:
                        setAmmeterN11I((float) value);
                        Logger.withTag("DEVICES_TAG").log("N11 I=" + ((float) value));
                        break;
                }
                break;
            case MU_ID:
                switch (param) {
                    case MUModel.RESPONDING_PARAM:
                        setMUResponding((boolean) value);
                        Logger.withTag("DEVICES_TAG").log("MUшка " + ((boolean) value ? "" : "не ") + "отвечает");
                        break;
                }
                break;
        }
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{param, value});
    }

    public void setPM130Responding(boolean PM130Responding) {
        mPM130Responding = PM130Responding;
    }

    public boolean isPM130Responding() {
        return mPM130Responding;
    }

    public void setPM130V2(float PM130V2) {
        mPM130V2 = PM130V2;
    }

    public void setPM130V3(float PM130V3) {
        mPM130V3 = PM130V3;
    }

    public void setPRControlResponding(boolean PRControlResponding) {
        mPRControlResponding = PRControlResponding;
    }

    public void setKM1State(boolean KM1State) {
        mKM1State = KM1State;
    }

    public void setObj1State(boolean obj1State) {
        mObj1State = obj1State;
    }

    public void setObj2State(boolean obj2State) {
        mObj2State = obj2State;
    }

    public void setObj3State(boolean obj3State) {
        mObj3State = obj3State;
    }

    public void setObj4State(boolean obj4State) {
        mObj4State = obj4State;
    }

    public void setKm2State(boolean km2State) {
        mKm2State = km2State;
    }

    public void setKm3State(boolean km3State) {
        mKm3State = km3State;
    }

    public void setKm4State(boolean km4State) {
        mKm4State = km4State;
    }

    public void setQ1State(boolean q1State) {
        mQ1State = q1State;
    }

    public void setSCState(boolean SCState) {
        mSCState = SCState;
    }

    public void setPRLatrResponding(boolean PRLatrResponding) {
        mPRLatrResponding = PRLatrResponding;
    }

    public void setOnlineAlarm(boolean onlineAlarm) {
        mOnlineAlarm = onlineAlarm;
    }

    public void setOnlineConcUp(boolean onlineConcUp) {
        mOnlineConcUp = onlineConcUp;
    }

    public void setOnlineConcDown(boolean onlineConcDown) {
        mOnlineConcDown = onlineConcDown;
    }

    public void setAmmeterN2Responding(boolean ammeterN2Responding) {
        mAmmeterN2Responding = ammeterN2Responding;
    }

    public void setAmmeterN2I(float ammeterN2I) {
        mAmmeterN2I = ammeterN2I;
    }

    public void setVoltmeterN3Responding(boolean voltmeterN3Responding) {
        mVoltmeterN3Responding = voltmeterN3Responding;
    }

    public void setVoltmeterN3U(float voltmeterN3U) {
        mVoltmeterN3U = voltmeterN3U;
    }

    public void setAmmeterN6Responding(boolean ammeterN6Responding) {
        mAmmeterN6Responding = ammeterN6Responding;
    }

    public void setAmmeterN6I(float ammeterN6I) {
        mAmmeterN6I = ammeterN6I;
    }

    public void setAmmeterN7Responding(boolean ammeterN7Responding) {
        mAmmeterN7Responding = ammeterN7Responding;
    }

    public void setAmmeterN7I(float ammeterN7I) {
        mAmmeterN7I = ammeterN7I;
    }

    public void setAmmeterN8Responding(boolean ammeterN8Responding) {
        mAmmeterN8Responding = ammeterN8Responding;
    }

    public void setAmmeterN8I(float ammeterN8I) {
        mAmmeterN8I = ammeterN8I;
    }

    public void setAmmeterN9Responding(boolean ammeterN9Responding) {
        mAmmeterN9Responding = ammeterN9Responding;
    }

    public void setAmmeterN9I(float ammeterN9I) {
        mAmmeterN9I = ammeterN9I;
    }

//    public void setVoltmeterN10Responding(boolean voltmeterN10Responding) {
//        mVoltmeterN10Responding = voltmeterN10Responding;
//    }

//    public void setVoltmeterN10U(float voltmeterN10U) {
//        mVoltmeterN10U = voltmeterN10U;
//    }

    public void setAmmeterN11Responding(boolean ammeterN11Responding) {
        mAmmeterN11Responding = ammeterN11Responding;
    }

    public void setAmmeterN11I(float ammeterN11I) {
        mAmmeterN11I = ammeterN11I;
    }

    public void setMUResponding(boolean MUResponding) {
        mMUResponding = MUResponding;
    }

    public float getPM130V2() {
        return mPM130V2;
    }

    public float getPM130V3() {
        return mPM130V3;
    }

    public boolean isPRControlResponding() {
        return mPRControlResponding;
    }

    public boolean isKM1State() {
        return mKM1State;
    }

    public boolean isObj1State() {
        return mObj1State;
    }

    public boolean isObj2State() {
        return mObj2State;
    }

    public boolean isObj3State() {
        return mObj3State;
    }

    public boolean isObj4State() {
        return mObj4State;
    }

    public boolean isKm2State() {
        return mKm2State;
    }

    public boolean isKm3State() {
        return mKm3State;
    }

    public boolean isKm4State() {
        return mKm4State;
    }

    public boolean isQ1State() {
        return mQ1State;
    }

    public boolean isSCState() {
        return mSCState;
    }

    public boolean isPRLatrResponding() {
        return mPRLatrResponding;
    }

    public boolean isOnlineAlarm() {
        return mOnlineAlarm;
    }

    public boolean isOnlineConcUp() {
        return mOnlineConcUp;
    }

    public boolean isOnlineConcDown() {
        return mOnlineConcDown;
    }

    public boolean isAmmeterN2Responding() {
        return mAmmeterN2Responding;
    }

    public float getAmmeterN2I() {
        return mAmmeterN2I;
    }

    public boolean isVoltmeterN3Responding() {
        return mVoltmeterN3Responding;
    }

    public float getVoltmeterN3U() {
        return mVoltmeterN3U;
    }

    public boolean isAmmeterN6Responding() {
        return mAmmeterN6Responding;
    }

    public float getAmmeterN6I() {
        return mAmmeterN6I;
    }

    public boolean isAmmeterN7Responding() {
        return mAmmeterN7Responding;
    }

    public float getAmmeterN7I() {
        return mAmmeterN7I;
    }

    public boolean isAmmeterN8Responding() {
        return mAmmeterN8Responding;
    }

    public float getAmmeterN8I() {
        return mAmmeterN8I;
    }

    public boolean isAmmeterN9Responding() {
        return mAmmeterN9Responding;
    }

    public float getAmmeterN9I() {
        return mAmmeterN9I;
    }

//    public boolean isVoltmeterN10Responding() {
//        return mVoltmeterN10Responding;
//    }

//    public float getVoltmeterN10U() {
//        return mVoltmeterN10U;
//    }

    public boolean isAmmeterN11Responding() {
        return mAmmeterN11Responding;
    }

    public float getAmmeterN11I() {
        return mAmmeterN11I;
    }

    public boolean isMUResponding() {
        return mMUResponding;
    }
}
