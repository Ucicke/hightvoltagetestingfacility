package ru.avem.highvoltagetestingfacility.presenter;

public interface ControlPanelView {
    boolean isControlPanelDisplayed();

    void hideControlPanel();

    void displayControlPanel();

    void displayDevicesStatePanel();

    void displayLogStatePanel();
}
