package ru.avem.highvoltagetestingfacility.presenter;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.Observable;
import java.util.Observer;

import ru.avem.highvoltagetestingfacility.R;
import ru.avem.highvoltagetestingfacility.communication.devices.DevicesController;
import ru.avem.highvoltagetestingfacility.database.model.Protocol;
import ru.avem.highvoltagetestingfacility.model.MainModel;
import ru.avem.highvoltagetestingfacility.utils.Logger;
import ru.avem.highvoltagetestingfacility.view.OnBroadcastCallback;

public class MainPresenter extends Observable {
    public static final int OBJECT_U_1 = 0;
    public static final int OBJECT_U_2 = 1;
    public static final int OBJECT_U_3 = 2;
    public static final int OBJECT_U_4 = 3;
    public static final int OBJECT_I_1 = 4;
    public static final int OBJECT_I_2 = 5;
    public static final int OBJECT_I_3 = 6;
    public static final int OBJECT_I_4 = 7;
    public static final int OBJECT_T_1 = 8;
    public static final int OBJECT_T_2 = 9;
    public static final int OBJECT_T_3 = 10;
    public static final int OBJECT_T_4 = 11;
    public static final int OBJECT_RESULT_1 = 12;
    public static final int OBJECT_RESULT_2 = 13;
    public static final int OBJECT_RESULT_3 = 14;
    public static final int OBJECT_RESULT_4 = 15;

    public static final String OBJECT_NULL = "Ничто";
    public static final String OBJECT_1 = "Перчатки";//6000 6мА 1мин КМ2 V=V_P3 I=P6-P9
    public static final String OBJECT_2 = "Боты";//15000 7.5мА 1мин КМ2 V=V_P3 I=P6-P9
    public static final String OBJECT_3 = "Галоши";//3500 2мА 1мин КМ3 V=V_PM130 I=P6-P9
    public static final String OBJECT_4 = "Индикаторы до 200 вольт";//вручную - - КМ4 V=V_PM130 I=P11
    public static final String OBJECT_5 = "Индикаторы до 3 киловольт";//вручную - - КМ3 V=V_PM130 I=P11
    public static final String OBJECT_6 = "Корпуса указателей напряжения до 500 вольт";//1000В - 1мин КМ3 V=V_PM130 I=P6-P9
    public static final String OBJECT_7 = "Слесарные инструменты и корпуса указателей напряжения выше 500 вольт";//2000В - 1мин КМ3 V=V_PM130 I=P6-P9
    public static final String OBJECT_8 = "Высоковольтные испытания на пробой";//вручную - 5мин КМ2 V=V_P3 I=P2
    public static final String OBJECT_9 = "Высоковольтные испытания на свето-звуковую индикацию";//вручную - - КМ2 V=V_P3 I=P2

    public static final String KM_NULL = "Нет";
    public static final String KM_2 = "KM_2";
    public static final String KM_3 = "KM_3";
    public static final String KM_4 = "KM_4";

    public static final String V_NULL = "Нет";
    public static final String V_P3 = "V_P3";
    public static final String V_PM130 = "V_PM130";

    public static final String A_NULL = "Нет";
    public static final String A_P6_P9 = "A_P6_P9";
    public static final String A_P11 = "A_P11";
    public static final String A_P2 = "A_P2";

    public static final int OK_RESULT = 0;
    public static final int BREAKDOWN_RESULT = 1;
    public static final int CONC_UP_RESULT = 2;

    private final MainPresenterView mView;
    private final MainModel mModel;

    private boolean mNeedToSave;

    private Protocol mProtocol;
    private String mObject = OBJECT_NULL;
    private int mSpecifiedU = -1;
    private float mOnlineU = -1;
    private float mObjectU1 = -1;
    private float mObjectU2 = -1;
    private float mObjectU3 = -1;
    private float mObjectU4 = -1;
    private float mSpecifiedI = -1;
    private float mOnlineI = -1;
    private float mObjectI1 = -1;
    private float mObjectI2 = -1;
    private float mObjectI3 = -1;
    private float mObjectI4 = -1;
    private int mSpecifiedT = -1;
    private int mObjectT1 = -1;
    private int mObjectT2 = -1;
    private int mObjectT3 = -1;
    private int mObjectT4 = -1;
    private int mOnlineT = -1;
    private int mObjectResult1 = -1;
    private int mObjectResult2 = -1;
    private int mObjectResult3 = -1;
    private int mObjectResult4 = -1;
    private boolean mElectrode1;
    private boolean mElectrode2;
    private boolean mElectrode3;
    private boolean mElectrode4;
    private int mNumOfSelected;
    private String mKM = KM_NULL;
    private String mVoltmeterDevice = V_NULL;
    private String mAmmeterDevice = A_NULL;

    private float mFixedU = -1;

    private float mStaticErrorI1;
    private float mStaticErrorI2;
    private float mStaticErrorI3;
    private float mStaticErrorI4;

    private DevicesController mDevicesController;
    private Protocol mProtocolForInteraction;

    public MainPresenter(MainPresenterView view, MainModel model, OnBroadcastCallback onBroadcastCallback, Observer observer) {
        mView = view;
        mModel = model;
        mDevicesController = new DevicesController((Context) mView, mModel, onBroadcastCallback);
        addObserver(observer);
    }

    public void activityReady() {
        mView.initializeViews();
    }

    public void exitPressed() {
        mView.finishApplication();
    }

    public void serialNumberEnterClicked(boolean checked) {
        if (checked) {
            String serialNumber = mView.getSerialNumber();
            if (!serialNumber.isEmpty()) {
                mProtocol = new Protocol(serialNumber);
                mView.showObjects(serialNumber);
            } else {
                mView.uncheckSerialNumberEnter();
                mView.toast("Введите заводской №");
            }
        } else {
            mProtocol = null;
            initObject();
            mView.hideExperimentsViews();
            mView.initializeObjectSelector();
            mView.uncheckSerialNumberEnter();
        }
    }

    private void initObject() {
        mObject = OBJECT_NULL;
        mSpecifiedU = -1;
        mOnlineU = -1;
        mObjectU1 = -1;
        mObjectU2 = -1;
        mObjectU3 = -1;
        mObjectU4 = -1;
        mSpecifiedI = -1;
        mOnlineI = -1;
        mObjectI1 = -1;
        mObjectI2 = -1;
        mObjectI3 = -1;
        mObjectI4 = -1;
        mSpecifiedT = -1;
        mObjectT1 = -1;
        mObjectT2 = -1;
        mObjectT3 = -1;
        mObjectT4 = -1;
        mOnlineT = -1;
        mObjectResult1 = -1;
        mObjectResult2 = -1;
        mObjectResult3 = -1;
        mObjectResult4 = -1;
        mKM = KM_NULL;
        mVoltmeterDevice = V_NULL;
        mAmmeterDevice = A_NULL;
    }

    public void showObjectParameters() {
        Logger.withTag(Logger.DEBUG_TAG).log("mObject " + mObject);
        Logger.withTag(Logger.DEBUG_TAG).log("mSpecifiedU " + mSpecifiedU);
        Logger.withTag(Logger.DEBUG_TAG).log("mOnlineU " + mOnlineU);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectU1 " + mObjectU1);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectU2 " + mObjectU2);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectU3 " + mObjectU3);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectU4 " + mObjectU4);
        Logger.withTag(Logger.DEBUG_TAG).log("mSpecifiedI " + mSpecifiedI);
        Logger.withTag(Logger.DEBUG_TAG).log("mOnlineI " + mOnlineI);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectI1 " + mObjectI1);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectI2 " + mObjectI2);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectI3 " + mObjectI3);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectI4 " + mObjectI4);
        Logger.withTag(Logger.DEBUG_TAG).log("mSpecifiedT " + mSpecifiedT);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectT1 " + mObjectT1);
        Logger.withTag(Logger.DEBUG_TAG).log("mOnlineT " + mOnlineT);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectResult1 " + mObjectResult1);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectResult2 " + mObjectResult2);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectResult3 " + mObjectResult3);
        Logger.withTag(Logger.DEBUG_TAG).log("mObjectResult4 " + mObjectResult4);
        Logger.withTag(Logger.DEBUG_TAG).log("mKM " + mKM);
        Logger.withTag(Logger.DEBUG_TAG).log("mVoltmeterDevice " + mVoltmeterDevice);
        Logger.withTag(Logger.DEBUG_TAG).log("mAmmeterDevice " + mAmmeterDevice);
    }

    public void resultsTabSelected() {
        mView.showConnectionWarning();
    }

    public void protocolTabSelected(Context context, Spinner protocols, long startDate, long endDate) {
        if (isNeedToSave()) {
            mView.showSaveLayout();
        } else {
            fillSpinnerFromDB(context, protocols, startDate, endDate);
            mView.showReviewLayout();
        }
    }

    private boolean isNeedToSave() {
        return mNeedToSave;
    }

    public void setNeedToSave(boolean needToSave) {
        mNeedToSave = needToSave;
    }

    private void fillSpinnerFromDB(Context context, Spinner spinner, long startDate, long endDate) {
        ArrayAdapter<Protocol> protocolArrayAdapter =
                new ArrayAdapter<>(context,
                        R.layout.spinner_layout,
                        mModel.getProtocolsByDateFromDB(startDate, endDate));
        spinner.setAdapter(protocolArrayAdapter);
    }

    public void setProtocolForInteraction(Protocol selectedItem) {
        mProtocolForInteraction = selectedItem;
    }

    public void objectSelected(String selectedObject) {
        setObject(selectedObject);
    }

    public void setObject(String object) {
        mObject = object;
        switch (object) {
            case OBJECT_1:
                mSpecifiedU = 6000;
                mSpecifiedI = 6;
                mSpecifiedT = 60;
                mKM = KM_2;
                mVoltmeterDevice = V_P3;
                mAmmeterDevice = A_P6_P9;
                break;
            case OBJECT_2:
                mSpecifiedU = 15000;
                mSpecifiedI = 7.5f;
                mSpecifiedT = 60;
                mKM = KM_2;
                mVoltmeterDevice = V_P3;
                mAmmeterDevice = A_P6_P9;
                break;
            case OBJECT_3:
                mSpecifiedU = 3500;
                mSpecifiedI = 2;
                mSpecifiedT = 60;
                mKM = KM_3;
                mVoltmeterDevice = V_PM130;
                mAmmeterDevice = A_P6_P9;
                break;
            case OBJECT_4:
                mSpecifiedU = -1;
                mSpecifiedI = 0;
                mSpecifiedT = 0;
                mKM = KM_4;
                mVoltmeterDevice = V_PM130;
                mAmmeterDevice = A_P11;
                break;
            case OBJECT_5:
                mSpecifiedU = -1;
                mSpecifiedI = 0;
                mSpecifiedT = 0;
                mKM = KM_3;
                mVoltmeterDevice = V_PM130;
                mAmmeterDevice = A_P11;
                break;
            case OBJECT_6:
                mSpecifiedU = 1000;
                mSpecifiedI = 0;
                mSpecifiedT = 60;
                mKM = KM_3;
                mVoltmeterDevice = V_PM130;
                mAmmeterDevice = A_P6_P9;
                break;
            case OBJECT_7:
                mSpecifiedU = 2000;
                mSpecifiedI = 0;
                mSpecifiedT = 60;
                mKM = KM_3;
                mVoltmeterDevice = V_PM130;
                mAmmeterDevice = A_P6_P9;
                break;
            case OBJECT_8:
                mSpecifiedU = -1;
                mSpecifiedI = 0;
                mSpecifiedT = 300;
                mKM = KM_2;
                mVoltmeterDevice = V_P3;
                mAmmeterDevice = A_P2;
                break;
            case OBJECT_9:
                mSpecifiedU = -1;
                mSpecifiedI = 0;
                mSpecifiedT = 0;
                mKM = KM_2;
                mVoltmeterDevice = V_P3;
                mAmmeterDevice = A_P2;
                break;
        }
        mView.setSpecifiedParam(object, mSpecifiedU, mSpecifiedI, mSpecifiedT);
    }

    public void changeSpecifiedU(String newSpecifiedU) {
        if (!newSpecifiedU.isEmpty()) {
            mSpecifiedU = Integer.parseInt(newSpecifiedU);
            if (mObject.equals(OBJECT_4) && (mSpecifiedU > 200)) {
                mView.disableObjectEnter();
            } else if (mObject.equals(OBJECT_5) && (mSpecifiedU > 3000)) {
                mView.disableObjectEnter();
            } else if ((mObject.equals(OBJECT_8) || mObject.equals(OBJECT_9)) && (mSpecifiedU > 100000)) {
                mView.disableObjectEnter();
            } else {
                mView.enableObjectEnter();
            }
        } else {
            mView.disableObjectEnter();
        }
    }

    public void selectElectrode(int numberOfElectrode, boolean checked) {
        mView.toast("Место подключения №" + numberOfElectrode + " " + (checked ? "выбрано" : "снято"));
        if (checked) {
            mNumOfSelected++;
        } else {
            mNumOfSelected--;
        }
        switch (numberOfElectrode) {
            case 1:
                mElectrode1 = checked;
                break;
            case 2:
                mElectrode2 = checked;
                break;
            case 3:
                mElectrode3 = checked;
                break;
            case 4:
                mElectrode4 = checked;
                break;
        }
        Logger.withTag("DEBUG_TAG").log(mNumOfSelected + " " + mElectrode1 + " " + mElectrode2 + " " + mElectrode3 + " " + mElectrode4);
        if (mObject.equals(OBJECT_2) && (mNumOfSelected == 2)) {
            mView.disableCheckboxes(!mElectrode1, !mElectrode2, !mElectrode3, !mElectrode4);
        } else {
            mView.disableCheckboxes(false, false, false, false);
        }
    }

    public void objectEnter() {
        if (mObject.equals(OBJECT_1)
                || mObject.equals(OBJECT_2)
                || mObject.equals(OBJECT_3)
                || mObject.equals(OBJECT_6)
                || mObject.equals(OBJECT_7)) {
            if (!(mElectrode1 || mElectrode2 || mElectrode3 || mElectrode4)) {
                mView.toast("Выберите хотя бы одно место подключения");
                return;
            }
        }
        mView.disableObjectTab();
        mView.showSpecifiedParams(mObject, mElectrode1, mElectrode2, mElectrode3, mElectrode4, mSpecifiedU, mSpecifiedI, mSpecifiedT);
        initExperiment();
    }

    public void objectCancel() {
        mDevicesController.diversifyDevices();
        mObjectResult1 = -1;
        mObjectResult2 = -1;
        mObjectResult3 = -1;
        mObjectResult4 = -1;
        mView.initializeSpecifiedParamsTables();
        mView.hideExperimentsViews();
        mView.enableObjectTab(mObject.equals(OBJECT_9));
        mView.checkEnabledU(mObject);
        if (mObject.equals(OBJECT_2) && (mNumOfSelected == 2)) {
            mView.disableCheckboxes(!mElectrode1, !mElectrode2, !mElectrode3, !mElectrode4);
        } else {
            mView.disableCheckboxes(false, false, false, false);
        }
    }

    public void objectNext() {
        mView.changeTabToExperiments();
    }

    private void initExperiment() {
        Logger.withTag("DEBUG_TAG").log("initExperiment");
        mFixedU = -1;
        mDevicesController.initMainGroupDevices();
        mDevicesController.initVoltmeterDevice(mVoltmeterDevice);
        mDevicesController.initAmmetersGroupDevices(mAmmeterDevice, mElectrode1, mElectrode2, mElectrode3, mElectrode4);

        new Thread(new Runnable() {
            @Override
            public void run() {
                mModel.setKM1State(false);
                int numOfAttempts = 5;
                while (!mModel.isPRControlResponding() && (--numOfAttempts > 0)) {
                    mDevicesController.initMainGroupDevices();
                    mDevicesController.initVoltmeterDevice(mVoltmeterDevice);
                    mDevicesController.initAmmetersGroupDevices(mAmmeterDevice, mElectrode1, mElectrode2, mElectrode3, mElectrode4);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException ignored) {
                    }
                }
                if (!mModel.isPRControlResponding() && (numOfAttempts <= 0)) {
                    mView.reportThatDevicesNotRespond();
                    return;
                }
                mDevicesController.resetTimer();
                while (!mModel.isKM1State()) {
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException ignored) {
                    }
                }
                mView.changeTabToResult();
                mDevicesController.controlToStart();
                mDevicesController.setObj(mElectrode1, mElectrode2, mElectrode3, mElectrode4);
                mDevicesController.onDoorLocking();
                mDevicesController.onCarriageLocking();
                switch (mKM) {
                    case KM_2:
                        mDevicesController.onShortCircuit();
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException ignored) {
                        }
                        mDevicesController.onKM2();
                        break;
                    case KM_3:
                        mDevicesController.onKM3();
                        break;
                    case KM_4:
                        mDevicesController.onKM4();
                        break;
                }
//                mStaticErrorI1 = mModel.getAmmeterN6I() + 0.15f;
//                Logger.withTag("DEBUG_TAG").log("SI1=" + mStaticErrorI1);
//                mStaticErrorI2 = mModel.getAmmeterN7I() + 0.15f;
//                Logger.withTag("DEBUG_TAG").log("SI2=" + mStaticErrorI2);
//                mStaticErrorI3 = mModel.getAmmeterN8I() + 0.15f;
//                Logger.withTag("DEBUG_TAG").log("SI3=" + mStaticErrorI3);
//                mStaticErrorI4 = mModel.getAmmeterN9I() + 0.15f;
//                Logger.withTag("DEBUG_TAG").log("SI4=" + mStaticErrorI4);
                if (mObject.equals(OBJECT_1) || mObject.equals(OBJECT_2)) {
                    mStaticErrorI1 = 0;
                    mStaticErrorI2 = 0;
                    mStaticErrorI3 = 0;
                    mStaticErrorI4 = 0;
                } else {
                    mStaticErrorI1 = mModel.getAmmeterN6I() + 0.15f;
                    mStaticErrorI2 = mModel.getAmmeterN7I() + 0.15f;
                    mStaticErrorI3 = mModel.getAmmeterN8I() + 0.15f;
                    mStaticErrorI4 = mModel.getAmmeterN9I() + 0.15f;
                }
                int result = startExperiment();
                switch (mKM) {
                    case KM_2:
                        mDevicesController.offKM2();
//                        try {
//                            Thread.sleep(200);
//                        } catch (InterruptedException ignored) {
//                        }
//                        mDevicesController.offShortCircuit();
                        break;
                    case KM_3:
                        mDevicesController.offKM3();
                        break;
                    case KM_4:
                        mDevicesController.offKM4();
                        break;
                }
                mDevicesController.offCarriageLocking();
                mDevicesController.offDoorLocking();
                mDevicesController.controlToEnd();
                mDevicesController.setObj(false, false, false, false);
                mDevicesController.diversifyDevices();
                if (result == OK_RESULT) {
                    Logger.withTag("DEBUG_TAG").log("endExperiment Успешно");
                    switch (mAmmeterDevice) {
                        case A_P2:
                            setObjectResult1(1);
                            break;
                        case A_P6_P9:
                            if ((mObjectResult1 == -1) && mElectrode1) {
                                setObjectResult1(1);
                            }
                            if ((mObjectResult2 == -1) && mElectrode2) {
                                setObjectResult2(1);
                            }
                            if ((mObjectResult3 == -1) && mElectrode3) {
                                setObjectResult3(1);
                            }
                            if ((mObjectResult4 == -1) && mElectrode4) {
                                setObjectResult4(1);
                            }
                            break;
                        case A_P11:
                            setObjectResult1(1);
                            break;
                    }
                } else if (result == BREAKDOWN_RESULT) {
                    switch (mAmmeterDevice) {
                        case A_P2:
                            setObjectResult1(0);
                            break;
                        case A_P6_P9:
                            if ((mObjectResult1 == -1) && mElectrode1) {
                                setObjectResult1(0);
                            }
                            if ((mObjectResult2 == -1) && mElectrode2) {
                                setObjectResult2(0);
                            }
                            if ((mObjectResult3 == -1) && mElectrode3) {
                                setObjectResult3(0);
                            }
                            if ((mObjectResult4 == -1) && mElectrode4) {
                                setObjectResult4(0);
                            }
                            break;
                        case A_P11:
                            setObjectResult1(0);
                            break;
                    }
                    Logger.withTag("DEBUG_TAG").log("endExperiment Неуспешно over5000");
                } else if (result == CONC_UP_RESULT) {
                    switch (mAmmeterDevice) {
                        case A_P2:
                            setObjectResult1(0);
                            break;
                        case A_P6_P9:
                            if ((mObjectResult1 == -1) && mElectrode1) {
                                setObjectResult1(0);
                            }
                            if ((mObjectResult2 == -1) && mElectrode2) {
                                setObjectResult2(0);
                            }
                            if ((mObjectResult3 == -1) && mElectrode3) {
                                setObjectResult3(0);
                            }
                            if ((mObjectResult4 == -1) && mElectrode4) {
                                setObjectResult4(0);
                            }
                            break;
                        case A_P11:
                            setObjectResult1(0);
                            break;
                    }
                    Logger.withTag("DEBUG_TAG").log("endExperiment Неуспешно concUp");
                }
                Logger.withTag("DEBUG_TAG").log("endExperiment");
                saveDataToTheProtocol();
                mView.showTheEndOfTestWindow(result);
            }
        }).start();
    }

    private int startExperiment() {
        try {
            Logger.withTag("DEBUG_TAG").log("startExperiment1");
            initLATR();

            int specifiedU = mSpecifiedU;
            Logger.withTag("DEBUG_TAG").log("specifiedU=" + specifiedU);
            int boundaries = 250;
            float koef = 0.7f;
            switch (mKM) {
                case KM_2:
                    boundaries = 250;
                    koef = 0.3f;
                    break;
                case KM_3:
                    boundaries = 40;
                    koef = 0.9f;
                    break;
                case KM_4:
                    boundaries = 5;
                    koef = 0.9f;
                    break;
            }
            int specifiedUMin = specifiedU - boundaries;
            int specifiedUMax = specifiedU + boundaries;

            mDevicesController.startUpLATR();
            switch (mVoltmeterDevice) {
                case V_P3:
                    while (!mModel.isOnlineConcUp() && ((mOnlineU = mModel.getVoltmeterN3U()) < (specifiedU * koef)) && !mModel.isOnlineAlarm() && (mFixedU == -1) && ((mObjectResult1 == -1) || ((mObjectResult2 == -1) && mElectrode2) || ((mObjectResult3 == -1) && mElectrode3) || ((mObjectResult4 == -1) && mElectrode4))) {
                        Logger.withTag("DEBUG_TAG").log("1mOnlineU=" + mOnlineU);
                        setObjectU1(mOnlineU);
                        setObjectU2(mOnlineU);
                        setObjectU3(mOnlineU);
                        setObjectU4(mOnlineU);
//                        switch (mAmmeterDevice) {
//                            case A_P2:
//                                setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
//                                break;
//                            case A_P6_P9:
//                                setObjectI1((mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1));
//                                setObjectI2((mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2));
//                                setObjectI3((mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3));
//                                setObjectI4((mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4));
//                                break;
//                            case A_P11:
//                                setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
//                                break;
//                        }
                        switch (mAmmeterDevice) {
                            case A_P2:
                                setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
                                break;
                            case A_P6_P9:
                                float correctN6I = (mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1);
                                if ((correctN6I > mSpecifiedI + 0.5f) || !mModel.isObj1State()) {
                                    setObjectResult1(0);
                                }
                                setObjectI1(correctN6I);

                                float correctN7I = (mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2);
                                if ((correctN7I > mSpecifiedI + 0.5f) || !mModel.isObj2State()) {
                                    setObjectResult2(0);
                                }
                                setObjectI2(correctN7I);

                                float correctN8I = (mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3);
                                if ((correctN8I > mSpecifiedI + 0.5f) || !mModel.isObj3State()) {
                                    setObjectResult3(0);
                                }
                                setObjectI3(correctN8I);

                                float correctN9I = (mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4);
                                if ((correctN9I > mSpecifiedI + 0.5f) || !mModel.isObj4State()) {
                                    setObjectResult4(0);
                                }
                                setObjectI4(correctN9I);
                                break;
                            case A_P11:
                                setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
                                break;
                        }
                        Thread.sleep(100);
                    }
                    break;
                case V_PM130:
                    while (!mModel.isOnlineConcUp() && ((mOnlineU = mModel.getPM130V3()) < (specifiedU * koef)) && !mModel.isOnlineAlarm() && (mFixedU == -1) && ((mObjectResult1 == -1) || ((mObjectResult2 == -1) && mElectrode2) || ((mObjectResult3 == -1) && mElectrode3) || ((mObjectResult4 == -1) && mElectrode4))) {
                        Logger.withTag("DEBUG_TAG").log("1mOnlineU=" + mOnlineU);
                        setObjectU1(mOnlineU);
                        setObjectU2(mOnlineU);
                        setObjectU3(mOnlineU);
                        setObjectU4(mOnlineU);
//                        switch (mAmmeterDevice) {
//                            case A_P2:
//                                setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
//                                break;
//                            case A_P6_P9:
//                                setObjectI1((mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1));
//                                setObjectI2((mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2));
//                                setObjectI3((mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3));
//                                setObjectI4((mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4));
//                                break;
//                            case A_P11:
//                                setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
//                                break;
//                        }
                        switch (mAmmeterDevice) {
                            case A_P2:
                                setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
                                break;
                            case A_P6_P9:
                                float correctN6I = (mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1);
                                if ((correctN6I > mSpecifiedI + 0.5f) || !mModel.isObj1State()) {
                                    setObjectResult1(0);
                                }
                                setObjectI1(correctN6I);

                                float correctN7I = (mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2);
                                if ((correctN7I > mSpecifiedI + 0.5f) || !mModel.isObj2State()) {
                                    setObjectResult2(0);
                                }
                                setObjectI2(correctN7I);

                                float correctN8I = (mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3);
                                if ((correctN8I > mSpecifiedI + 0.5f) || !mModel.isObj3State()) {
                                    setObjectResult3(0);
                                }
                                setObjectI3(correctN8I);

                                float correctN9I = (mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4);
                                if ((correctN9I > mSpecifiedI + 0.5f) || !mModel.isObj4State()) {
                                    setObjectResult4(0);
                                }
                                setObjectI4(correctN9I);
                                break;
                            case A_P11:
                                setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
                                break;
                        }
                        Thread.sleep(100);
                    }
                    break;
            }

            mDevicesController.stopLATR();
            if (mModel.isOnlineAlarm()) {
                return BREAKDOWN_RESULT;
            }
            if (mModel.isOnlineConcUp()) {
                return CONC_UP_RESULT;
            }

            switch (mVoltmeterDevice) {
                case V_P3:
                    while ((((mOnlineU = mModel.getVoltmeterN3U()) < specifiedUMin) || (mOnlineU > specifiedUMax)) && !mModel.isOnlineAlarm() && (mFixedU == -1) && ((mObjectResult1 == -1) || ((mObjectResult2 == -1) && mElectrode2) || ((mObjectResult3 == -1) && mElectrode3) || ((mObjectResult4 == -1) && mElectrode4))) {
                        Logger.withTag("DEBUG_TAG").log("2mOnlineU=" + mOnlineU);
                        setObjectU1(mOnlineU);
                        setObjectU2(mOnlineU);
                        setObjectU3(mOnlineU);
                        setObjectU4(mOnlineU);
//                        switch (mAmmeterDevice) {
//                            case A_P2:
//                                setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
//                                break;
//                            case A_P6_P9:
//                                setObjectI1((mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1));
//                                setObjectI2((mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2));
//                                setObjectI3((mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3));
//                                setObjectI4((mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4));
//                                break;
//                            case A_P11:
//                                setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
//                                break;
//                        }
                        switch (mAmmeterDevice) {
                            case A_P2:
                                setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
                                break;
                            case A_P6_P9:
                                float correctN6I = (mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1);
                                if ((correctN6I > mSpecifiedI + 0.5f) || !mModel.isObj1State()) {
                                    setObjectResult1(0);
                                }
                                setObjectI1(correctN6I);

                                float correctN7I = (mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2);
                                if ((correctN7I > mSpecifiedI + 0.5f) || !mModel.isObj2State()) {
                                    setObjectResult2(0);
                                }
                                setObjectI2(correctN7I);

                                float correctN8I = (mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3);
                                if ((correctN8I > mSpecifiedI + 0.5f) || !mModel.isObj3State()) {
                                    setObjectResult3(0);
                                }
                                setObjectI3(correctN8I);

                                float correctN9I = (mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4);
                                if ((correctN9I > mSpecifiedI + 0.5f) || !mModel.isObj4State()) {
                                    setObjectResult4(0);
                                }
                                setObjectI4(correctN9I);
                                break;
                            case A_P11:
                                setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
                                break;
                        }
                        if (mOnlineU < specifiedUMin) {
                            mDevicesController.startUpLATR();
                        } else if (mOnlineU > specifiedUMax) {
                            mDevicesController.startDownLATR();
                        }
                        Thread.sleep(20);
                        mDevicesController.stopLATR();
                        Thread.sleep(200);
                    }
                    break;
                case V_PM130:
                    while ((((mOnlineU = mModel.getPM130V3()) < specifiedUMin) || (mOnlineU > specifiedUMax)) && !mModel.isOnlineAlarm() && (mFixedU == -1) && ((mObjectResult1 == -1) || ((mObjectResult2 == -1) && mElectrode2) || ((mObjectResult3 == -1) && mElectrode3) || ((mObjectResult4 == -1) && mElectrode4))) {
                        Logger.withTag("DEBUG_TAG").log("2mOnlineU=" + mOnlineU);
                        setObjectU1(mOnlineU);
                        setObjectU2(mOnlineU);
                        setObjectU3(mOnlineU);
                        setObjectU4(mOnlineU);
//                        switch (mAmmeterDevice) {
//                            case A_P2:
//                                setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
//                                break;
//                            case A_P6_P9:
//                                setObjectI1((mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1));
//                                setObjectI2((mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2));
//                                setObjectI3((mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3));
//                                setObjectI4((mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4));
//                                break;
//                            case A_P11:
//                                setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
//                                break;
//                        }
                        switch (mAmmeterDevice) {
                            case A_P2:
                                setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
                                break;
                            case A_P6_P9:
                                float correctN6I = (mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1);
                                if ((correctN6I > mSpecifiedI + 0.5f) || !mModel.isObj1State()) {
                                    setObjectResult1(0);
                                }
                                setObjectI1(correctN6I);

                                float correctN7I = (mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2);
                                if ((correctN7I > mSpecifiedI + 0.5f) || !mModel.isObj2State()) {
                                    setObjectResult2(0);
                                }
                                setObjectI2(correctN7I);

                                float correctN8I = (mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3);
                                if ((correctN8I > mSpecifiedI + 0.5f) || !mModel.isObj3State()) {
                                    setObjectResult3(0);
                                }
                                setObjectI3(correctN8I);

                                float correctN9I = (mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4);
                                if ((correctN9I > mSpecifiedI + 0.5f) || !mModel.isObj4State()) {
                                    setObjectResult4(0);
                                }
                                setObjectI4(correctN9I);
                                break;
                            case A_P11:
                                setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
                                break;
                        }
                        if (mOnlineU < specifiedUMin) {
                            mDevicesController.startUpLATR();
                        } else if (mOnlineU > specifiedUMax) {
                            mDevicesController.startDownLATR();
                        }
                        Thread.sleep(20);
                        mDevicesController.stopLATR();
                        Thread.sleep(200);
                    }
                    break;
            }
            Logger.withTag("DEBUG_TAG").log("3mOnlineU=" + mOnlineU);
            setObjectU1(mOnlineU);
            setObjectU2(mOnlineU);
            setObjectU3(mOnlineU);
            setObjectU4(mOnlineU);


            mDevicesController.stopLATR();
            if (mModel.isOnlineAlarm()) {
                return BREAKDOWN_RESULT;
            }
            if (mObject.equals(OBJECT_4) || mObject.equals(OBJECT_5) || mObject.equals(OBJECT_9)) {
                if ((mFixedU != -1)) {
                    return OK_RESULT;
                } else {
                    return CONC_UP_RESULT;
                }
            }
            int t = mSpecifiedT * 10;
            while (!mModel.isOnlineAlarm() && (--t >= 0) && (mObjectResult1 == -1 || ((mObjectResult2 == -1) && mElectrode2) || ((mObjectResult3 == -1) && mElectrode3) || ((mObjectResult4 == -1) && mElectrode4))) {
                Thread.sleep(100);
                setObjectU1(mOnlineU);
                setObjectU2(mOnlineU);
                setObjectU3(mOnlineU);
                setObjectU4(mOnlineU);
                switch (mAmmeterDevice) {
                    case A_P2:
                        setObjectI1((mModel.getAmmeterN2I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN2I() - mStaticErrorI1));
                        break;
                    case A_P6_P9:
                        float correctN6I = (mModel.getAmmeterN6I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN6I() - mStaticErrorI1);
                        if ((correctN6I > mSpecifiedI + 0.5f) || !mModel.isObj1State()) {
                            setObjectResult1(0);
                        }
                        setObjectI1(correctN6I);

                        float correctN7I = (mModel.getAmmeterN7I() - mStaticErrorI2) < 0 ? 0 : (mModel.getAmmeterN7I() - mStaticErrorI2);
                        if ((correctN7I > mSpecifiedI + 0.5f) || !mModel.isObj2State()) {
                            setObjectResult2(0);
                        }
                        setObjectI2(correctN7I);

                        float correctN8I = (mModel.getAmmeterN8I() - mStaticErrorI3) < 0 ? 0 : (mModel.getAmmeterN8I() - mStaticErrorI3);
                        if ((correctN8I > mSpecifiedI + 0.5f) || !mModel.isObj3State()) {
                            setObjectResult3(0);
                        }
                        setObjectI3(correctN8I);

                        float correctN9I = (mModel.getAmmeterN9I() - mStaticErrorI4) < 0 ? 0 : (mModel.getAmmeterN9I() - mStaticErrorI4);
                        if ((correctN9I > mSpecifiedI + 0.5f) || !mModel.isObj4State()) {
                            setObjectResult4(0);
                        }
                        setObjectI4(correctN9I);
                        break;
                    case A_P11:
                        setObjectI1((mModel.getAmmeterN11I() - mStaticErrorI1) < 0 ? 0 : (mModel.getAmmeterN11I() - mStaticErrorI1));
                        break;
                }
                setObjectT1(t / 10);
                setObjectT2(t / 10);
                setObjectT3(t / 10);
                setObjectT4(t / 10);
            }
            if (mModel.isOnlineAlarm()) {
                return BREAKDOWN_RESULT;
            }
            if (!returnToStart()) {
                return BREAKDOWN_RESULT;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return OK_RESULT;
    }

    private void initLATR() {
        if (!mModel.isOnlineConcDown()) {
            returnToStart();
        }
        mDevicesController.offTransfKmLATR();
    }

    private boolean returnToStart() {
        mDevicesController.startDownLATR();
        while (!mModel.isOnlineConcDown() && !mModel.isOnlineAlarm()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mDevicesController.stopWithOffTransfKmLATR();
        return !mModel.isOnlineAlarm();
    }

    private void saveDataToTheProtocol() {
        mProtocol.setObjectName(mObject);
        mProtocol.setSpecifiedU(mSpecifiedU);
        mProtocol.setObjectU1(mObjectU1);
        mProtocol.setObjectU2(mObjectU2);
        mProtocol.setObjectU3(mObjectU3);
        mProtocol.setObjectU4(mObjectU4);
        mProtocol.setSpecifiedI(mSpecifiedI);
        mProtocol.setObjectI1(mObjectI1);
        mProtocol.setObjectI2(mObjectI2);
        mProtocol.setObjectI3(mObjectI3);
        mProtocol.setObjectI4(mObjectI4);
        mProtocol.setSpecifiedT(mSpecifiedT);
        mProtocol.setObjectResult1(mObjectResult1);
        mProtocol.setObjectResult2(mObjectResult2);
        mProtocol.setObjectResult3(mObjectResult3);
        mProtocol.setObjectResult4(mObjectResult4);
    }

    public void fixResult() {
        setObjectU1(mOnlineU);
        mFixedU = mOnlineU;
    }

    public void setObjectU1(float objectU1) {
        if ((mFixedU == -1) && (mObjectResult1 == -1)) {
            mObjectU1 = objectU1;
            notice(OBJECT_U_1, objectU1);
        }
    }

    public void setObjectU2(float objectU2) {
        if (mObjectResult2 == -1) {
            mObjectU2 = objectU2;
            notice(OBJECT_U_2, objectU2);
        }
    }

    public void setObjectU3(float objectU3) {
        if (mObjectResult3 == -1) {
            mObjectU3 = objectU3;
            notice(OBJECT_U_3, objectU3);
        }
    }

    public void setObjectU4(float objectU4) {
        if (mObjectResult4 == -1) {
            mObjectU4 = objectU4;
            notice(OBJECT_U_4, objectU4);
        }
    }

    public void setObjectI1(float objectI1) {
        if ((mFixedU == -1) && (mObjectResult1 == -1)) {
            mObjectI1 = objectI1;
            notice(OBJECT_I_1, objectI1);
        }
    }

    public void setObjectI2(float objectI2) {
        if (mObjectResult2 == -1) {
            mObjectI2 = objectI2;
            notice(OBJECT_I_2, objectI2);
        }
    }

    public void setObjectI3(float objectI3) {
        if (mObjectResult3 == -1) {
            mObjectI3 = objectI3;
            notice(OBJECT_I_3, objectI3);
        }
    }

    public void setObjectI4(float objectI4) {
        if (mObjectResult4 == -1) {
            mObjectI4 = objectI4;
            notice(OBJECT_I_4, objectI4);
        }
    }

    public void setObjectT1(int objectT1) {
        if ((mFixedU == -1) && (mObjectResult1 == -1)) {
            mObjectT1 = objectT1;
            notice(OBJECT_T_1, objectT1);
        }
    }

    public void setObjectT2(int objectT2) {
        if (mObjectResult2 == -1) {
            mObjectT2 = objectT2;
            notice(OBJECT_T_2, objectT2);
        }
    }

    public void setObjectT3(int objectT3) {
        if (mObjectResult3 == -1) {
            mObjectT3 = objectT3;
            notice(OBJECT_T_3, objectT3);
        }
    }

    public void setObjectT4(int objectT4) {
        if (mObjectResult4 == -1) {
            mObjectT4 = objectT4;
            notice(OBJECT_T_4, objectT4);
        }
    }

    public void setObjectResult1(int objectResult1) {
        if (mObjectResult1 == -1) {
            mObjectResult1 = objectResult1;
            notice(OBJECT_RESULT_1, objectResult1);
        }
    }

    public void setObjectResult2(int objectResult2) {
        if (mObjectResult2 == -1) {
            mObjectResult2 = objectResult2;
            notice(OBJECT_RESULT_2, objectResult2);
        }
    }

    public void setObjectResult3(int objectResult3) {
        if (mObjectResult3 == -1) {
            mObjectResult3 = objectResult3;
            notice(OBJECT_RESULT_3, objectResult3);
        }
    }

    public void setObjectResult4(int objectResult4) {
        if (mObjectResult4 == -1) {
            mObjectResult4 = objectResult4;
            notice(OBJECT_RESULT_4, objectResult4);
        }
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{param, value});
    }

    public Protocol getProtocolForInteraction() {
        return mProtocolForInteraction;
    }

    public void saveProtocolInDB(String position1, String position1Number, String position1FullName, String position2, String position2Number, String position2FullName, long date) {
        mProtocol.setPosition1(position1);
        mProtocol.setPosition1Number(position1Number);
        mProtocol.setPosition1FullName(position1FullName);
        mProtocol.setPosition2(position2);
        mProtocol.setPosition2Number(position2Number);
        mProtocol.setPosition2FullName(position2FullName);
        mModel.saveProtocolToDB(mProtocol);
    }

    public void saveProtocolToDB() {
        mModel.saveProtocolToDB(new Protocol(0, "SN448", "Перчатки2",
                6000,
                6154.1234f, 6154.1234f, 6164.9934f, 6154.1234f,
                6f,
                2f, 2.2f, 7f, 0.4f,
                60,
                1, 1, 0, 1,
                "Глава", "1", "Путин В.В.",
                "ВРИО", "2", "Медведев Д.А.",
                System.currentTimeMillis()));
    }
}
