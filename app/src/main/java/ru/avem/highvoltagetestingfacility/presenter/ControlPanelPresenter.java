package ru.avem.highvoltagetestingfacility.presenter;

public class ControlPanelPresenter {
    private final ControlPanelView mView;

    public ControlPanelPresenter(ControlPanelView view) {
        mView = view;
    }

    public void protectionStatusClicked() {
    }

    public void subjectClicked() {
    }

    public void eventsClicked() {
    }

    public void devicesStateClicked() {
        mView.displayDevicesStatePanel();
    }

    public void dataTransferClicked() {
        mView.displayLogStatePanel();
    }

    public void switcherClicked() {
        if (mView.isControlPanelDisplayed()) {
            mView.hideControlPanel();
        } else {
            mView.displayControlPanel();
        }
    }
}
