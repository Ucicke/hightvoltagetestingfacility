package ru.avem.highvoltagetestingfacility.presenter;

public interface MainPresenterView {
    void finishApplication();

    void initializeViews();

    void showConnectionWarning();

    void showSaveLayout();

    void showReviewLayout();

    String getSerialNumber();

    void showObjects(String serialNumber);

    void hideObjects();

    void toast(String s);

    void uncheckSerialNumberEnter();

    void setSpecifiedParam(String object, int specifiedU, float specifiedI, int specifiedT);

    void enableObjectEnter();

    void disableObjectEnter();

    void disableCheckboxes(boolean checkbox1, boolean checkbox2, boolean checkbox3, boolean checkbox4);

    void disableObjectTab();

    void enableObjectTab(boolean equals);

    void checkEnabledU(String object);

    void changeTabToExperiments();

    void showSpecifiedParams(String object, boolean electrode1, boolean electrode2, boolean electrode3, boolean electrode4, int specifiedU, float specifiedI, int specifiedT);

    void initializeSpecifiedParamsTables();

    void changeTabToResult();

    void reportThatDevicesNotRespond();

    void initializeObjectSelector();

    void showTheEndOfTestWindow(int result);

    void hideExperimentsViews();
}
