package ru.avem.highvoltagetestingfacility.logging;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.github.mjdev.libaums.fs.FileSystem;
import com.github.mjdev.libaums.fs.UsbFile;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;

import ru.avem.highvoltagetestingfacility.R;
import ru.avem.highvoltagetestingfacility.database.model.Protocol;

import static android.content.Context.USB_SERVICE;

public class Logging {
    private static final String ACTION_USB_PERMISSION =
            "ru.avem.coilstestingfacility.USB_PERMISSION";

    public static void preview(Activity activity, Protocol protocol) {
        if (requestPermission(activity)) {
            new SaveTask(2, activity).execute(protocol);
        } else {
            Toast.makeText(activity, "Ошибка доступа. Дайте разрешение на запись.", Toast.LENGTH_SHORT).show();
        }
    }

    private static boolean requestPermission(Activity activity) {
        boolean hasPermission = (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    112);
            return false;
        } else {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/protocol";
            File storageDir = new File(path);
            if (!storageDir.exists() && !storageDir.mkdirs()) {
                return false;
            }
        }
        return true;
    }

    private static class SaveTask extends AsyncTask<Protocol, Void, String> {
        private ProgressDialog dialog;
        private int mType;
        private final Context mContext;

        SaveTask(int type, Context context) {
            mType = type;
            mContext = context;
            dialog = new ProgressDialog(context);
            if (mType == 1) {
                dialog.setMessage("Идёт сохранение...");
            } else if (mType == 2) {
                dialog.setMessage("Подождите...");
            }
            dialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected String doInBackground(Protocol... protocols) {
            String fileName = null;
            Protocol protocol = protocols[0];
            if (protocol != null) {
                if (mType == 1) {
                    fileName = writeWorkbookToMassStorage(protocol, mContext);
                } else if (mType == 2) {
                    fileName = writeWorkbookToInternalStorage(protocol, mContext);
                }
            }
            return fileName;
        }

        @Override
        protected void onPostExecute(String fileName) {
            super.onPostExecute(fileName);
            dialog.dismiss();
            if (fileName != null) {
                if (mType == 1) {
                    Toast.makeText(mContext, "Сохранено в " + fileName, Toast.LENGTH_SHORT).show();
                } else if (mType == 2) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    File file = new File(fileName);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    Uri uri = FileProvider.getUriForFile(mContext,
                            mContext.getApplicationContext().getPackageName() + ".provider",
                            file);
                    String mimeType = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                            "xlsx");
                    intent.setDataAndType(uri, mimeType);
                    mContext.startActivity(intent);
                }
            } else {
                Toast.makeText(mContext, "Выберите протокол из выпадающего списка", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private static String writeWorkbookToMassStorage(Protocol protocol, Context context) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM(HH-mm-ss)");
        String fileName = "protocol-" + sdf.format(System.currentTimeMillis()) + ".xlsx";
        UsbMassStorageDevice[] massStorageDevices = UsbMassStorageDevice.getMassStorageDevices(context);
        UsbMassStorageDevice currentDevice = massStorageDevices[0];
        try {
            currentDevice.init();
            FileSystem currentFS = currentDevice.getPartitions().get(0).getFileSystem();

            UsbFile root = currentFS.getRootDirectory();
            UsbFile file = root.createFile(fileName);
            ByteArrayOutputStream out = convertProtocolToWorkbook(protocol, context);
            file.write(0, ByteBuffer.wrap(out.toByteArray()));
            file.close();
            fileName = currentFS.getVolumeLabel() + "/" + fileName;
            currentDevice.close();
        } catch (IOException e) {
            Log.e("TAG", "setup device error", e);
        }
        return fileName;
    }

    private static ByteArrayOutputStream convertProtocolToWorkbook(Protocol protocol, Context context) throws IOException {
        Resources res = context.getResources();
        InputStream inputStream = res.openRawResource(R.raw.template_hvtf);
        Workbook wb = new XSSFWorkbook(inputStream);
        try {
            Sheet sheet = wb.getSheetAt(0);
            for (int i = 0; i < 1000; i++) {
                Row row = sheet.getRow(i);
                if (row != null) {
                    for (int j = 0; j < 1000; j++) {
                        Cell cell = row.getCell(j);
                        if (cell != null && (cell.getCellTypeEnum() == CellType.STRING)) {
                            switch (cell.getStringCellValue()) {
                                case "$PROTOCOL_NUMBER$":
                                    long id = protocol.getId();
                                    if (id != 0) {
                                        cell.setCellValue(id + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$OBJECT$":
                                    String objectName = protocol.getObjectName();
                                    if (!objectName.equals("Ничто")) {
                                        cell.setCellValue(objectName);
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$SERIAL_NUMBER$":
                                    String serialNumber = protocol.getSerialNumber();
                                    if (!serialNumber.isEmpty()) {
                                        cell.setCellValue(serialNumber);
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_SET_1$":
                                    int specifiedUTC1 = protocol.getSpecifiedU();
                                    if ((specifiedUTC1 != -1) && (protocol.getObjectResult1() != -1)) {
                                        cell.setCellValue(String.format("%d", specifiedUTC1));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_SET_2$":
                                    int specifiedUTC2 = protocol.getSpecifiedU();
                                    if ((specifiedUTC2 != -1) && (protocol.getObjectResult2() != -1)) {
                                        cell.setCellValue(String.format("%d", specifiedUTC2));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_SET_3$":
                                    int specifiedUTC3 = protocol.getSpecifiedU();
                                    if ((specifiedUTC3 != -1) && (protocol.getObjectResult3() != -1)) {
                                        cell.setCellValue(String.format("%d", specifiedUTC3));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_SET_4$":
                                    int specifiedUTC4 = protocol.getSpecifiedU();
                                    if ((specifiedUTC4 != -1) && (protocol.getObjectResult4() != -1)) {
                                        cell.setCellValue(String.format("%d", specifiedUTC4));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_OBJ_1$":
                                    float objectU1 = protocol.getObjectU1();
                                    if ((objectU1 != -1) && (protocol.getObjectResult1() != -1)) {
                                        cell.setCellValue(String.format("%.2f", objectU1));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_OBJ_2$":
                                    float objectU2 = protocol.getObjectU2();
                                    if ((objectU2 != -1) && (protocol.getObjectResult2() != -1)) {
                                        cell.setCellValue(String.format("%.2f", objectU2));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_OBJ_3$":
                                    float objectU3 = protocol.getObjectU3();
                                    if ((objectU3 != -1) && (protocol.getObjectResult3() != -1)) {
                                        cell.setCellValue(String.format("%.2f", objectU3));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_OBJ_4$":
                                    float objectU4 = protocol.getObjectU4();
                                    if ((objectU4 != -1) && (protocol.getObjectResult4() != -1)) {
                                        cell.setCellValue(String.format("%.2f", objectU4));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_SET_1$":
                                    float specifiedI1 = protocol.getSpecifiedI();
                                    if ((specifiedI1 != -1) && (protocol.getObjectResult1() != -1)) {
                                        cell.setCellValue(String.format("%.2f", specifiedI1));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_SET_2$":
                                    float specifiedI2 = protocol.getSpecifiedI();
                                    if ((specifiedI2 != -1) && (protocol.getObjectResult2() != -1)) {
                                        cell.setCellValue(String.format("%.2f", specifiedI2));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_SET_3$":
                                    float specifiedI3 = protocol.getSpecifiedI();
                                    if ((specifiedI3 != -1) && (protocol.getObjectResult3() != -1)) {
                                        cell.setCellValue(String.format("%.2f", specifiedI3));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_SET_4$":
                                    float specifiedI4 = protocol.getSpecifiedI();
                                    if ((specifiedI4 != -1) && (protocol.getObjectResult4() != -1)) {
                                        cell.setCellValue(String.format("%.2f", specifiedI4));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_OBJ_1$":
                                    float objectI1 = protocol.getObjectI1();
                                    if ((objectI1 != -1) && (protocol.getObjectResult1() != -1)) {
                                        cell.setCellValue(String.format("%.2f", objectI1));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_OBJ_2$":
                                    float objectI2 = protocol.getObjectI2();
                                    if ((objectI2 != -1) && (protocol.getObjectResult2() != -1)) {
                                        cell.setCellValue(String.format("%.2f", objectI2));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_OBJ_3$":
                                    float objectI3 = protocol.getObjectI3();
                                    if ((objectI3 != -1) && (protocol.getObjectResult3() != -1)) {
                                        cell.setCellValue(String.format("%.2f", objectI3));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_OBJ_4$":
                                    float objectI4 = protocol.getObjectI4();
                                    if ((objectI4 != -1) && (protocol.getObjectResult4() != -1)) {
                                        cell.setCellValue(String.format("%.2f", objectI4));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$TIME_1$":
                                    int time1 = protocol.getSpecifiedT();
                                    if ((time1 != -1) && (protocol.getObjectResult1() != -1)) {
                                        cell.setCellValue(String.format("%d", time1));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$TIME_2$":
                                    int time2 = protocol.getSpecifiedT();
                                    if ((time2 != -1) && (protocol.getObjectResult2() != -1)) {
                                        cell.setCellValue(String.format("%d", time2));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$TIME_3$":
                                    int time3 = protocol.getSpecifiedT();
                                    if ((time3 != -1) && (protocol.getObjectResult3() != -1)) {
                                        cell.setCellValue(String.format("%d", time3));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$TIME_4$":
                                    int time4 = protocol.getSpecifiedT();
                                    if ((time4 != -1) && (protocol.getObjectResult4() != -1)) {
                                        cell.setCellValue(String.format("%d", time4));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$RESULT_1$":
                                    int objectResult1 = protocol.getObjectResult1();
                                    if (objectResult1 != -1) {
                                        cell.setCellValue(String.format("%s", (objectResult1 == 1) ? "Успешно" : "Неуспешно"));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$RESULT_2$":
                                    int objectResult2 = protocol.getObjectResult2();
                                    if (objectResult2 != -1) {
                                        cell.setCellValue(String.format("%s", (objectResult2 == 1) ? "Успешно" : "Неуспешно"));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$RESULT_3$":
                                    int objectResult3 = protocol.getObjectResult3();
                                    if (objectResult3 != -1) {
                                        cell.setCellValue(String.format("%s", (objectResult3 == 1) ? "Успешно" : "Неуспешно"));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$RESULT_4$":
                                    int objectResult4 = protocol.getObjectResult4();
                                    if (objectResult4 != -1) {
                                        cell.setCellValue(String.format("%s", (objectResult4 == 1) ? "Успешно" : "Неуспешно"));
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$POS_1$":
                                    cell.setCellValue(protocol.getPosition1());
                                    break;
                                case "$POS_2$":
                                    cell.setCellValue(protocol.getPosition2());
                                    break;
                                case "$POS_1_NAME$":
                                    cell.setCellValue("/" + protocol.getPosition1FullName() + "/");
                                    break;
                                case "$POS_2_NAME$":
                                    cell.setCellValue("/" + protocol.getPosition2FullName() + "/");
                                    break;
                                case "$DATE$":
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
                                    cell.setCellValue(sdf.format(protocol.getDate()));
                                    break;
                            }
                        }
                    }
                }
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                wb.write(out);
            } finally {
                out.close();
            }
            return out;
        } finally {
            wb.close();
        }
    }

    private static String writeWorkbookToInternalStorage(Protocol protocol, Context context) {
        clearDirectory(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/protocol"));
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM(HH-mm-ss)");
        String fileName = "protocol-" + sdf.format(System.currentTimeMillis()) + ".xlsx";
        try {
            ByteArrayOutputStream out = convertProtocolToWorkbook(protocol, context);
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/protocol", fileName);
            FileOutputStream fileOut = new FileOutputStream(file);
            out.writeTo(fileOut);
            fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/protocol/" + fileName;
            out.close();
            fileOut.close();
        } catch (IOException e) {
            Log.e("TAG", " error", e);
        }
        return fileName;
    }

    private static void clearDirectory(File directory) {
        for (File child : directory.listFiles()) {
            child.delete();
        }
    }

    public static void saveFileOnFlashMassStorage(Context context, Protocol protocol) {
        if (checkMassStorageConnection(context)) {
            new SaveTask(1, context).execute(protocol);
        } else {
            new AlertDialog.Builder(
                    context)
                    .setTitle("Нет подключения")
                    .setCancelable(false)
                    .setMessage("Подключите USB FLASH накопитель с файловой системой FAT32 и предоставьте доступ к нему")
                    .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .create()
                    .show();
        }
    }

    private static boolean checkMassStorageConnection(Context context) {
        UsbMassStorageDevice[] massStorageDevices = UsbMassStorageDevice.getMassStorageDevices(context);
        if (massStorageDevices.length != 1) {
            return false;
        } else {
            UsbManager usbManager = (UsbManager) context.getSystemService(USB_SERVICE);
            if (usbManager.hasPermission(massStorageDevices[0].getUsbDevice())) {
                return true;
            } else {
                PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
                usbManager.requestPermission(massStorageDevices[0].getUsbDevice(), pi);
                return false;
            }
        }
    }

    public static boolean isDeviceFlashMassStorage(UsbDevice device) {
        int interfaceCount = device.getInterfaceCount();
        for (int i = 0; i < interfaceCount; i++) {
            UsbInterface usbInterface = device.getInterface(i);
            int INTERFACE_SUBCLASS = 6;
            int INTERFACE_PROTOCOL = 80;
            if (usbInterface.getInterfaceClass() == UsbConstants.USB_CLASS_MASS_STORAGE
                    && usbInterface.getInterfaceSubclass() == INTERFACE_SUBCLASS
                    && usbInterface.getInterfaceProtocol() == INTERFACE_PROTOCOL) {
                return true;
            }
        }
        return false;
    }
}